const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

async function main() {
  await prisma.role.deleteMany();
  // Add default roles
  await prisma.role.upsert({
    where: {
      id: 1,
    },
    create: {
      id: 1,
      name: "admin",
      description: "Admin role",
    },
    update: {},
  });
  await prisma.role.upsert({
    where: {
      id: 2,
    },
    create: {
      id: 2,
      name: "source-manager",
      description: "Source manager role",
    },
    update: {},
  });
  await prisma.role.upsert({
    where: {
      id: 3,
    },
    create: {
      id: 3,
      name: "policy-manager",
      description: "Policy manager role",
    },
    update: {},
  });
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
