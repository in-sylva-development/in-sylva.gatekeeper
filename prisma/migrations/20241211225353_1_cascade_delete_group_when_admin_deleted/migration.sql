-- DropForeignKey
ALTER TABLE "groups" DROP CONSTRAINT "groups_admin_id_fkey";

-- AddForeignKey
ALTER TABLE "groups" ADD CONSTRAINT "groups_admin_id_fkey" FOREIGN KEY ("admin_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;
