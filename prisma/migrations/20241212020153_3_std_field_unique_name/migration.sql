/*
  Warnings:

  - A unique constraint covering the columns `[field_name]` on the table `std_fields` will be added. If there are existing duplicate values, this will fail.
  - Made the column `field_name` on table `std_fields` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "std_fields" ALTER COLUMN "field_name" SET NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "std_fields_field_name_key" ON "std_fields"("field_name");
