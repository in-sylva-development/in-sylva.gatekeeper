-- AlterTable
ALTER TABLE "source_indices" ALTER COLUMN "index_id" DROP NOT NULL,
ALTER COLUMN "mng_id" DROP NOT NULL,
ALTER COLUMN "is_send" SET DEFAULT false;
