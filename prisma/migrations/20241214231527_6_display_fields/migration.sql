-- CreateTable
CREATE TABLE "display_std_fields" (
    "user_id" INTEGER NOT NULL,
    "std_field_id" INTEGER NOT NULL,

    CONSTRAINT "display_std_fields_pkey" PRIMARY KEY ("user_id","std_field_id")
);

-- AddForeignKey
ALTER TABLE "display_std_fields" ADD CONSTRAINT "display_std_fields_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "display_std_fields" ADD CONSTRAINT "display_std_fields_std_field_id_fkey" FOREIGN KEY ("std_field_id") REFERENCES "std_fields"("id") ON DELETE CASCADE ON UPDATE CASCADE;
