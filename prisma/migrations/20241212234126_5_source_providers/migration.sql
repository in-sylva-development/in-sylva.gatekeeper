-- CreateTable
CREATE TABLE "sources_users" (
    "source_id" INTEGER NOT NULL,
    "user_id" INTEGER NOT NULL,

    CONSTRAINT "sources_users_pkey" PRIMARY KEY ("source_id","user_id")
);

-- AddForeignKey
ALTER TABLE "sources_users" ADD CONSTRAINT "sources_users_source_id_fkey" FOREIGN KEY ("source_id") REFERENCES "sources"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "sources_users" ADD CONSTRAINT "sources_users_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;
