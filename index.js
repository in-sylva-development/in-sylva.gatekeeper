"use strict";

require("module-alias/register");

const dotenv = require("dotenv");
if (process.env.NODE_ENV === "development") {
  dotenv.config({ silent: true });
}

require("./web");
