"use strict";

const Koa = require("koa");
const logger = require("@/app/utils/format");
const api = require("./app/api");
const koaLogger = require("koa-logger");
const cors = require("koa2-cors");
const { isAccessTokenValid } = require("./app/dal/authService");

async function responseTime(ctx, next) {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set("X-Response-Time", `${ms}ms`);
}

const app = new Koa();

app.proxy = false;

app
  .use(
    cors({
      origin: "*",
    })
  )
  .use(koaLogger())
  .use(async (ctx, next) => {
    if (ctx.path === "/healthcheck") {
      ctx.status = 200;
      ctx.body = { message: "Healthcheck OK" };
      return;
    }
    const { authorization } = ctx.request.headers;
    if (!authorization) {
      ctx.status = 401;
      ctx.body = {
        message: "Bearer token is required in Authorization header",
      };
    } else {
      const accessToken = authorization.split(" ")[1];
      const isValid = await isAccessTokenValid(accessToken);
      if (!isValid) {
        ctx.status = 401;
        ctx.body = {
          message: "Invalid access token",
        };
      } else {
        await next();
      }
    }
  })
  .use(api.routes())
  .use(api.allowedMethods())
  .use(responseTime);

app.on("error", (err) => {
  logger.error("Server error", { error: err.message });
  throw err;
});

module.exports = app;
