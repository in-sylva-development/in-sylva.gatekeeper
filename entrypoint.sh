#!/bin/bash

echo -n "Generating Prisma client... "
yarn prisma generate
echo "OK"

echo -n "Running migrations... "
yarn prisma migrate deploy
echo "OK"

echo -n "Seeding database... "
yarn prisma db seed
echo "OK"

exec "$@"
