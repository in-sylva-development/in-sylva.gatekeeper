jest.mock("@/app/utils/format", () => ({
  info: jest.fn(),
  error: jest.fn(),
  debug: jest.fn(),
}));

jest.mock("@/app/dal/elasticService", () => ({
  prepareForBulk: jest.fn(),
  bulk: jest.fn(),
  countByIndex: jest.fn(),
  search: jest.fn(),
}));

jest.mock("@/app/dal/mongoService", () => ({
  addSourceToMongoDB: jest.fn(),
  removeSourceFromMongoDB: jest.fn(),
}));
