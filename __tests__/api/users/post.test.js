// post.test.js

const { getOrCreateUserHandler } = require("@/app/api/users/post");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const { createUser, getUser } = require("@/app/dal/userService");
const prisma = require("@/prisma/client");

jest.mock("@/prisma/client", () => ({
  user: {
    findFirst: jest.fn(),
    create: jest.fn(),
  },
}));

jest.mock("@/app/dal/userService");
jest.mock("@/app/utils/errorHandler");

describe("getOrCreateUserHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when kc_id is missing", async () => {
    ctx.request.body = { email: "test@example.com" };

    await getOrCreateUserHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["kc_id"], ctx);
  });

  it("should handle missing parameters when email is missing", async () => {
    ctx.request.body = { kc_id: "kc123" };

    await getOrCreateUserHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["email"], ctx);
  });

  it("should handle missing parameters when both kc_id and email are missing", async () => {
    ctx.request.body = {};

    await getOrCreateUserHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["kc_id", "email"],
      ctx
    );
  });

  it("should return user data and set status to 200 if user exists", async () => {
    const mockUser = { id: 1, kc_id: "kc123", email: "test@example.com" };
    ctx.request.body = { kc_id: "kc123", email: "test@example.com" };
    getUser.mockResolvedValue(mockUser);

    await getOrCreateUserHandler(ctx);

    expect(ctx.body).toEqual(mockUser);
    expect(ctx.status).toBe(200);
  });

  it("should create a new user and set status to 201 if user does not exist", async () => {
    const mockUser = { id: 1, kc_id: "kc123", email: "test@example.com" };
    ctx.request.body = { kc_id: "kc123", email: "test@example.com" };
    getUser.mockResolvedValue(null);
    createUser.mockResolvedValue(mockUser);

    prisma.user.findFirst.mockResolvedValue(null);
    await getOrCreateUserHandler(ctx);

    expect(ctx.body).toEqual(mockUser);
    expect(ctx.status).toBe(201);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.request.body = { kc_id: "kc123", email: "test@example.com" };
    createUser.mockImplementation(() => {
      throw error;
    });

    await getOrCreateUserHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
