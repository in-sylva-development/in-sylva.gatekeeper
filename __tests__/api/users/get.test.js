"use strict";
const { getUsersHandler, getUserHandler } = require("@/app/api/users/get");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const { getUsers, getUser } = require("@/app/dal/userService");

jest.mock("@/app/utils/errorHandler");
jest.mock("@/app/dal/userService");

describe("User Handlers", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      body: null,
      status: null,
    };
  });

  describe("getUsersHandler", () => {
    it("should return all users and set status to 200", async () => {
      const mockUsers = [
        { id: 1, name: "User 1" },
        { id: 2, name: "User 2" },
      ];
      getUsers.mockResolvedValue(mockUsers);

      await getUsersHandler(ctx);

      expect(ctx.body).toEqual(mockUsers);
      expect(ctx.status).toBe(200);
    });

    it("should handle internal error", async () => {
      const error = new Error("Internal Error");
      getUsers.mockImplementation(() => {
        throw error;
      });

      await getUsersHandler(ctx);

      expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
    });
  });

  describe("getUserHandler", () => {
    it("should handle missing parameters", async () => {
      await getUserHandler(ctx);
      expect(handleMissingParameters).toHaveBeenCalledWith(["kc_id"], ctx);
    });

    it("should return user data and set status to 200", async () => {
      const mockUser = { id: 1, name: "Test User" };
      ctx.params.kc_id = 1;
      getUser.mockResolvedValue(mockUser);

      await getUserHandler(ctx);

      expect(ctx.body).toEqual(mockUser);
      expect(ctx.status).toBe(200);
    });

    it("should handle internal error", async () => {
      const error = new Error("Internal Error");
      ctx.params.kc_id = 1;
      getUser.mockImplementation(() => {
        throw error;
      });

      await getUserHandler(ctx);

      expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
    });
  });
});
