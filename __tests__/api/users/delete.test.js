const { deleteUserHandler } = require("@/app/api/users/delete");
const userService = require("@/app/dal/userService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/userService");
jest.mock("@/app/utils/errorHandler");

describe("deleteUserHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when kc_id is missing", async () => {
    await deleteUserHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["kc_id"], ctx);
  });

  it("should delete user and set status to 204", async () => {
    ctx.params = { kc_id: "kc123" };
    userService.deleteUser.mockResolvedValue({});

    await deleteUserHandler(ctx);

    expect(ctx.body).toEqual({});
    expect(ctx.status).toBe(204);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params = { kc_id: "kc123" };
    userService.deleteUser.mockImplementation(() => {
      throw error;
    });

    await deleteUserHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
