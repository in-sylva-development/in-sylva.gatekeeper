const { deleteSourceHandler } = require("@/app/api/sources/delete");
const { deleteSource } = require("@/app/dal/sourceService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/sourceService");
jest.mock("@/app/utils/errorHandler");

describe("deleteSourceHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id is missing", async () => {
    await deleteSourceHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["id"], ctx);
  });

  it("should delete source and set status to 200", async () => {
    ctx.params.id = "source123";
    const mockResponse = { success: true };
    deleteSource.mockResolvedValue(mockResponse);

    await deleteSourceHandler(ctx);

    expect(ctx.body).toEqual(null);
    expect(ctx.status).toBe(204);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "source123";
    deleteSource.mockImplementation(() => {
      throw error;
    });

    await deleteSourceHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
