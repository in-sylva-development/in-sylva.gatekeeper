const { createSourceHandler } = require("@/app/api/sources/post");
const { createSource } = require("@/app/dal/sourceService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/sourceService");
jest.mock("@/app/utils/errorHandler");

describe("createSourceHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when name, description, metaUrfms and kc_id are missing", async () => {
    await createSourceHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["name", "description", "kc_id", "metaUrfms"],
      ctx
    );
  });

  it("should create source and set status to 201", async () => {
    ctx.request.body = {
      name: "A name",
      description: "A description",
      metaUrfms: "metaUrfms",
      kc_id: "kc_id",
    };
    const mockResponse = {
      id: "source123",
      name: "A name",
      description: "A description",
    };
    createSource.mockResolvedValue(mockResponse);

    await createSourceHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(201);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.request.body = {
      name: "A name",
      description: "A description",
      metaUrfms: "metaUrfms",
      kc_id: "kc_id",
    };
    createSource.mockImplementation(() => {
      throw error;
    });

    await createSourceHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
