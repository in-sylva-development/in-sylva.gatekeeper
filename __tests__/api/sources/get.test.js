const {
  getSourcesHandler,
  getSourceHandler,
  getSourcesByProviderHandler,
  getIndexedSourcesHandler,
  getIndexedSourcesByProviderHandler,
} = require("@/app/api/sources/get");
const {
  getSources,
  getSource,
  getSourcesByProvider,
  getIndexedSources,
  getIndexedSourcesByProvider,
} = require("@/app/dal/sourceService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/sourceService");
jest.mock("@/app/utils/errorHandler");

describe("getSourcesHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      body: null,
      status: null,
    };
  });

  it("should get sources and set status to 200", async () => {
    const mockResponse = [{ id: "source1" }, { id: "source2" }];
    getSources.mockResolvedValue(mockResponse);

    await getSourcesHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    getSources.mockImplementation(() => {
      throw error;
    });

    await getSourcesHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("getSourceHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id is missing", async () => {
    await getSourceHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["id"], ctx);
  });

  it("should get source by id and set status to 200", async () => {
    ctx.params.id = "source123";
    const mockResponse = { id: "source123", name: "Source Name" };
    getSource.mockResolvedValue(mockResponse);

    await getSourceHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "source123";
    getSource.mockImplementation(() => {
      throw error;
    });

    await getSourceHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("getSourcesByProviderHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when kc_id is missing", async () => {
    await getSourcesByProviderHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["kc_id"], ctx);
  });

  it("should get sources by provider and set status to 200", async () => {
    ctx.params.kc_id = "kc123";
    const mockResponse = [{ id: "source1" }, { id: "source2" }];
    getSourcesByProvider.mockResolvedValue(mockResponse);

    await getSourcesByProviderHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.kc_id = "kc123";
    getSourcesByProvider.mockImplementation(() => {
      throw error;
    });

    await getSourcesByProviderHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("getIndexedSourcesHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      body: null,
      status: null,
    };
  });

  it("should get indexed sources and set status to 200", async () => {
    const mockResponse = [{ id: "source1" }, { id: "source2" }];
    getIndexedSources.mockResolvedValue(mockResponse);

    await getIndexedSourcesHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    getIndexedSources.mockImplementation(() => {
      throw error;
    });

    await getIndexedSourcesHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("getIndexedSourcesByProviderHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when kc_id is missing", async () => {
    await getIndexedSourcesByProviderHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["kc_id"], ctx);
  });

  it("should get indexed sources by provider and set status to 200", async () => {
    ctx.params.kc_id = "kc123";
    const mockResponse = [{ id: "source1" }, { id: "source2" }];
    getIndexedSourcesByProvider.mockResolvedValue(mockResponse);

    await getIndexedSourcesByProviderHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.kc_id = "kc123";
    getIndexedSourcesByProvider.mockImplementation(() => {
      throw error;
    });

    await getIndexedSourcesByProviderHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
