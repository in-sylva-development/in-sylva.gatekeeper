const { updateStdFieldHandler } = require("@/app/api/std_fields/update");
const { createOrUpdateStdField } = require("@/app/dal/stdFieldService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/stdFieldService");
jest.mock("@/app/utils/errorHandler");

describe("updateStdFieldHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id is missing", async () => {
    await updateStdFieldHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["id"], ctx);
  });

  it("should update standard field and set status to 204", async () => {
    ctx.params.id = "field123";
    ctx.request.body = { name: "Updated Field" };
    const mockResponse = { id: "field123", name: "Updated Field" };
    createOrUpdateStdField.mockResolvedValue(mockResponse);

    await updateStdFieldHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(204);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "field123";
    ctx.request.body = { name: "Updated Field" };
    createOrUpdateStdField.mockImplementation(() => {
      throw error;
    });

    await updateStdFieldHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
