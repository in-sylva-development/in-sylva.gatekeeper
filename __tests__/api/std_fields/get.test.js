// get.test.js

const {
  getStdFieldsHandler,
  getStdFieldHandler,
} = require("@/app/api/std_fields/get");
const { getStdFields, getStdField } = require("@/app/dal/stdFieldService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/stdFieldService");
jest.mock("@/app/utils/errorHandler");

describe("getStdFieldsHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      body: null,
      status: null,
    };
  });

  it("should get standard fields and set status to 200", async () => {
    const mockResponse = [{ id: "field1" }, { id: "field2" }];
    getStdFields.mockResolvedValue(mockResponse);

    await getStdFieldsHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    getStdFields.mockImplementation(() => {
      throw error;
    });

    await getStdFieldsHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("getStdFieldHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id is missing", async () => {
    await getStdFieldHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["id"], ctx);
  });

  it("should get standard field by id and set status to 200", async () => {
    ctx.params.id = "field123";
    const mockResponse = { name: "Field Name" };
    getStdField.mockResolvedValue(mockResponse);

    await getStdFieldHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "field123";
    getStdField.mockImplementation(() => {
      throw error;
    });

    await getStdFieldHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
