const { createOrUpdateStdFieldHandler } = require("@/app/api/std_fields/post");
const { createOrUpdateStdField } = require("@/app/dal/stdFieldService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/stdFieldService");
jest.mock("@/app/utils/errorHandler");

describe("createOrUpdateStdFieldHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should create standard field and set status to 201", async () => {
    ctx.request.body.name = "New Field";
    const mockResponse = { id: "field123", name: "New Field" };
    createOrUpdateStdField.mockResolvedValue(mockResponse);

    await createOrUpdateStdFieldHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(201);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.request.body.name = "New Field";
    createOrUpdateStdField.mockImplementation(() => {
      throw error;
    });

    await createOrUpdateStdFieldHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
