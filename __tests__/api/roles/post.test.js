const {
  createRoleHandler,
  addUserToRoleHandler,
  removeUserFromRoleHandler,
} = require("@/app/api/roles/post");
const {
  createRole,
  addUserToRole,
  removeUserFromRole,
} = require("@/app/dal/roleService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/roleService");
jest.mock("@/app/utils/errorHandler");

describe("createRoleHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when name and description are missing", async () => {
    await createRoleHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["name", "description"],
      ctx
    );
  });

  it("should handle missing parameters when name is missing", async () => {
    ctx.request.body.description = "A description";

    await createRoleHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["name"], ctx);
  });

  it("should handle missing parameters when description is missing", async () => {
    ctx.request.body.name = "A name";

    await createRoleHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["description"], ctx);
  });

  it("should create role and set status to 201", async () => {
    ctx.request.body = { name: "A name", description: "A description" };
    const mockResponse = {
      id: "role123",
      name: "A name",
      description: "A description",
    };
    createRole.mockResolvedValue(mockResponse);

    await createRoleHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(201);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.request.body = { name: "A name", description: "A description" };
    createRole.mockImplementation(() => {
      throw error;
    });

    await createRoleHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("addUserToRoleHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id is missing", async () => {
    ctx.request.body.kc_id = "kc123";

    await addUserToRoleHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["id"], ctx);
  });

  it("should handle missing parameters when kc_id is missing", async () => {
    ctx.params.id = "role123";

    await addUserToRoleHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["kc_id"], ctx);
  });

  it("should add user to role and set status to 201", async () => {
    ctx.params.id = "role123";
    ctx.request.body.kc_id = "kc123";
    const mockResponse = {
      id: "role123",
      kc_id: "kc123",
    };
    addUserToRole.mockResolvedValue(mockResponse);

    await addUserToRoleHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(201);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "role123";
    ctx.request.body.kc_id = "kc123";
    addUserToRole.mockImplementation(() => {
      throw error;
    });

    await addUserToRoleHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("removeUserFromRoleHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id is missing", async () => {
    ctx.params.kc_id = "kc123";

    await removeUserFromRoleHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["id"], ctx);
  });

  it("should handle missing parameters when kc_id is missing", async () => {
    ctx.params.id = "role123";

    await removeUserFromRoleHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["kc_id"], ctx);
  });

  it("should remove user from role and set status to 204", async () => {
    ctx.params.id = "role123";
    ctx.params.kc_id = "kc123";

    await removeUserFromRoleHandler(ctx);

    expect(ctx.status).toBe(204);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "role123";
    ctx.params.kc_id = "kc123";
    removeUserFromRole.mockImplementation(() => {
      throw error;
    });

    await removeUserFromRoleHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
