const { updateRoleHandler } = require("@/app/api/roles/put");
const { updateRole } = require("@/app/dal/roleService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/roleService");
jest.mock("@/app/utils/errorHandler");

describe("updateRoleHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id, name, and description are missing", async () => {
    await updateRoleHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["id", "name", "description"],
      ctx
    );
  });

  it("should handle missing parameters when id is missing", async () => {
    ctx.request.body = { name: "Role Name", description: "Role Description" };

    await updateRoleHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["id"], ctx);
  });

  it("should handle missing parameters when name is missing", async () => {
    ctx.params.id = "role123";
    ctx.request.body.description = "Role Description";

    await updateRoleHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["name"], ctx);
  });

  it("should handle missing parameters when description is missing", async () => {
    ctx.params.id = "role123";
    ctx.request.body.name = "Role Name";

    await updateRoleHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["description"], ctx);
  });

  it("should update role and set status to 204", async () => {
    ctx.params.id = "role123";
    ctx.request.body = { name: "Role Name", description: "Role Description" };
    const mockResponse = {
      id: "role123",
      name: "Role Name",
      description: "Role Description",
    };
    updateRole.mockResolvedValue(mockResponse);

    await updateRoleHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(204);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "role123";
    ctx.request.body = { name: "Role Name", description: "Role Description" };
    updateRole.mockImplementation(() => {
      throw error;
    });

    await updateRoleHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
