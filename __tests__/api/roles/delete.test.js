const { deleteRoleHandler } = require("@/app/api/roles/delete");
const { deleteRole } = require("@/app/dal/roleService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/roleService");
jest.mock("@/app/utils/errorHandler");

describe("deleteRoleHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id is missing", async () => {
    await deleteRoleHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["id"], ctx);
  });

  it("should delete role and set status to 204", async () => {
    ctx.params.id = "role123";
    deleteRole.mockResolvedValue(null);

    await deleteRoleHandler(ctx);

    expect(ctx.body).toBeNull();
    expect(ctx.status).toBe(204);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "role123";
    deleteRole.mockImplementation(() => {
      throw error;
    });

    await deleteRoleHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
