const {
  getRolesHandler,
  getRoleHandler,
  getUserRolesHandler,
} = require("@/app/api/roles/get");
const { getRoles, getRole, getUserRoles } = require("@/app/dal/roleService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/roleService");
jest.mock("@/app/utils/errorHandler");

describe("getRolesHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      body: null,
      status: null,
    };
  });

  it("should get roles and set status to 200", async () => {
    const mockResponse = [{ id: "role1" }, { id: "role2" }];
    getRoles.mockResolvedValue(mockResponse);

    await getRolesHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    getRoles.mockImplementation(() => {
      throw error;
    });

    await getRolesHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("getRoleHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id is missing", async () => {
    await getRoleHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["id"], ctx);
  });

  it("should get role by id and set status to 200", async () => {
    ctx.params.id = "role123";
    const mockResponse = { id: "role123", name: "Role Name" };
    getRole.mockResolvedValue(mockResponse);

    await getRoleHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "role123";
    getRole.mockImplementation(() => {
      throw error;
    });

    await getRoleHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("getUserRolesHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when kc_id is missing", async () => {
    await getUserRolesHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["kc_id"], ctx);
  });

  it("should get user roles by kc_id and set status to 200", async () => {
    ctx.request.body.kc_id = "kc123";
    const mockResponse = [{ id: "role1" }, { id: "role2" }];
    getUserRoles.mockResolvedValue(mockResponse);

    await getUserRolesHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.request.body.kc_id = "kc123";
    getUserRoles.mockImplementation(() => {
      throw error;
    });

    await getUserRolesHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
