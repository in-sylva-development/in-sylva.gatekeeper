const {
  getAllPoliciesHandler,
  getPolicyHandler,
} = require("@/app/api/policies/get");
const { getAllPolicies, getPolicy } = require("@/app/dal/policyService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/policyService");
jest.mock("@/app/utils/errorHandler");

describe("getAllPoliciesHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      body: null,
      status: null,
    };
  });

  it("should get all policies and set status to 200", async () => {
    const mockResponse = [
      { id: 1, name: "Policy 1" },
      { id: 2, name: "Policy 2" },
    ];
    getAllPolicies.mockResolvedValue(mockResponse);

    await getAllPoliciesHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    getAllPolicies.mockImplementation(() => {
      throw error;
    });

    await getAllPoliciesHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("getPolicyHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id is missing", async () => {
    await getPolicyHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["id"], ctx);
  });

  it("should get policy and set status to 200", async () => {
    ctx.params.id = "policy123";
    const mockResponse = { id: "policy123", name: "Test Policy" };
    getPolicy.mockResolvedValue(mockResponse);

    await getPolicyHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "policy123";
    getPolicy.mockImplementation(() => {
      throw error;
    });

    await getPolicyHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
