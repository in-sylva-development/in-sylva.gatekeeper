// update.test.js

const {
  updatePolicyHandler,
  addFieldToPolicyHandler,
  removeFieldFromPolicyHandler,
  addSourceToPolicyHandler,
  removeSourceFromPolicyHandler,
} = require("@/app/api/policies/update");
const {
  updatePolicy,
  addFieldToPolicy,
  removeFieldFromPolicy,
  addSourceToPolicy,
  removeSourceFromPolicy,
} = require("@/app/dal/policyService");
const {
  handleMissingParameters,
  handleInternalError,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/policyService");
jest.mock("@/app/utils/errorHandler");

describe("updatePolicyHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      request: {
        body: {},
      },
      params: {},
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id, name, or isDefault is missing", async () => {
    await updatePolicyHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["id", "name", "isDefault"],
      ctx
    );
  });

  it("should update policy and set status to 204", async () => {
    ctx.params.id = "policy123";
    ctx.request.body = {
      name: "Updated Policy",
      isDefault: true,
    };
    const mockResponse = {
      id: "policy123",
      name: "Updated Policy",
      isDefault: true,
    };
    updatePolicy.mockResolvedValue(mockResponse);

    await updatePolicyHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(204);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "policy123";
    ctx.request.body = {
      name: "Updated Policy",
      isDefault: true,
    };
    updatePolicy.mockImplementation(() => {
      throw error;
    });

    await updatePolicyHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("addFieldToPolicyHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id or field_id is missing", async () => {
    await addFieldToPolicyHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["id", "field_id"],
      ctx
    );
  });

  it("should add field to policy and set status to 201", async () => {
    ctx.params.id = "policy123";
    ctx.request.body.field_id = "field123";
    const mockResponse = { success: true };
    addFieldToPolicy.mockResolvedValue(mockResponse);

    await addFieldToPolicyHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(201);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "policy123";
    ctx.request.body.field_id = "field123";
    addFieldToPolicy.mockImplementation(() => {
      throw error;
    });

    await addFieldToPolicyHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("removeFieldFromPolicyHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id or policy_id is missing", async () => {
    await removeFieldFromPolicyHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["id", "field_id"],
      ctx
    );
  });

  it("should remove field from policy and set status to 204", async () => {
    ctx.params.id = "policy123";
    ctx.params.field_id = "field123";
    const mockResponse = { success: true };
    removeFieldFromPolicy.mockResolvedValue(mockResponse);

    await removeFieldFromPolicyHandler(ctx);

    expect(ctx.status).toBe(204);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "policy123";
    ctx.params.field_id = "field123";
    removeFieldFromPolicy.mockImplementation(() => {
      throw error;
    });

    await removeFieldFromPolicyHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("addSourceToPolicyHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id or source_id is missing", async () => {
    await addSourceToPolicyHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["id", "source_id"],
      ctx
    );
  });

  it("should add source to policy and set status to 201", async () => {
    ctx.params.id = "policy123";
    ctx.request.body.source_id = "source123";
    const mockResponse = { success: true };
    addSourceToPolicy.mockResolvedValue(mockResponse);

    await addSourceToPolicyHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(201);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "policy123";
    ctx.request.body.source_id = "source123";
    addSourceToPolicy.mockImplementation(() => {
      throw error;
    });

    await addSourceToPolicyHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("removeSourceFromPolicyHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id or source_id is missing", async () => {
    await removeSourceFromPolicyHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["id", "source_id"],
      ctx
    );
  });

  it("should add source to policy and set status to 201", async () => {
    ctx.params = { id: "policy123", source_id: "source123" };
    removeSourceFromPolicy.mockResolvedValue(null);

    await removeSourceFromPolicyHandler(ctx);

    expect(ctx.body).toEqual(null);
    expect(ctx.status).toBe(204);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params = { id: "policy123", source_id: "source123" };
    removeSourceFromPolicy.mockImplementation(() => {
      throw error;
    });

    await removeSourceFromPolicyHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
