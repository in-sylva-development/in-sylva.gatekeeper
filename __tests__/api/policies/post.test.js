const { createPolicyHandler } = require("@/app/api/policies/post");
const { createPolicy } = require("@/app/dal/policyService");
const {
  handleMissingParameters,
  handleInternalError,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/policyService");
jest.mock("@/app/utils/errorHandler");

describe("createPolicyHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when name, source_id, or kc_id is missing", async () => {
    await createPolicyHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["name", "kc_id"],
      ctx
    );
  });

  it("should create policy and set status to 201", async () => {
    ctx.request.body = {
      name: "Test Policy",
      kc_id: "kc123",
    };
    const mockResponse = {
      id: 1,
      name: "Test Policy",
      kc_id: "kc123",
    };
    createPolicy.mockResolvedValue(mockResponse);

    await createPolicyHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(201);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.request.body = {
      name: "Test Policy",
      source_id: "source123",
      kc_id: "kc123",
    };
    createPolicy.mockImplementation(() => {
      throw error;
    });

    await createPolicyHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
