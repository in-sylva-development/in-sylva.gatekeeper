// delete.test.js

const { deletePolicyHandler } = require("@/app/api/policies/delete");
const { deletePolicy } = require("@/app/dal/policyService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/policyService");
jest.mock("@/app/utils/errorHandler");

describe("deletePolicyHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id is missing", async () => {
    await deletePolicyHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["id"], ctx);
  });

  it("should delete policy and set status to 204", async () => {
    ctx.params.id = "policy123";
    deletePolicy.mockResolvedValue(null);

    await deletePolicyHandler(ctx);

    expect(ctx.body).toBeNull();
    expect(ctx.status).toBe(204);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "policy123";
    deletePolicy.mockImplementation(() => {
      throw error;
    });

    await deletePolicyHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
