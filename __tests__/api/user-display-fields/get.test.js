const {
  handleInternalError,
  handleMissingParameters,
} = require("../../../app/utils/errorHandler");
const {
  getUserDisplayFields,
} = require("../../../app/dal/userDisplayFieldsService");
const {
  getUserDisplayFieldsHandler,
} = require("../../../app/api/user-display-fields/get");

jest.mock("@/app/dal/userService");
jest.mock("@/app/dal/userDisplayFieldsService");
jest.mock("@/app/utils/errorHandler");

describe("getUserDisplayFieldsHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when kc_id is missing", async () => {
    await getUserDisplayFieldsHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["kc_id"], ctx);
  });

  it("should get user display fields and set status to 200", async () => {
    ctx.params.kc_id = "user123";
    const mockResponse = { field1: "value1", field2: "value2" };
    getUserDisplayFields.mockResolvedValue(mockResponse);

    await getUserDisplayFieldsHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.kc_id = "user123";
    getUserDisplayFields.mockImplementation(() => {
      throw error;
    });

    await getUserDisplayFieldsHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
