// post.test.js

const {
  addSearchHistoryToUserHandler,
} = require("@/app/api/user-search-history/post");
const { addSearchHistoryToUser } = require("@/app/dal/searchHistoryService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/searchHistoryService");
jest.mock("@/app/utils/errorHandler");

describe("addSearchHistoryToUserHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when any required parameter is missing", async () => {
    ctx.request.body = { query: "test query" };

    await addSearchHistoryToUserHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["kc_id", "name", "ui_structure", "description"],
      ctx
    );
  });

  it("should add search history and set status to 201", async () => {
    const mockResponse = { id: 1, query: "test query" };

    ctx.params = { kc_id: "test-id" };
    ctx.request.body = {
      query: "test query",
      name: "test name",
      ui_structure: "test structure",
      description: "test description",
    };
    addSearchHistoryToUser.mockResolvedValue(mockResponse);

    await addSearchHistoryToUserHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(201);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params = { kc_id: "test-id" };
    ctx.request.body = {
      query: "test query",
      name: "test name",
      ui_structure: "test structure",
      description: "test description",
    };
    addSearchHistoryToUser.mockImplementation(() => {
      throw error;
    });

    await addSearchHistoryToUserHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
