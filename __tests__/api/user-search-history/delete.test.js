// delete.test.js

const {
  deleteHistoryHandler,
  deleteAllHistoryByUserHandler,
} = require("@/app/api/user-search-history/delete");
const {
  deleteHistory,
  deleteAllHistoryByUser: deleteAllHistoryByUserService,
} = require("@/app/dal/searchHistoryService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/searchHistoryService");
jest.mock("@/app/utils/errorHandler");

describe("deleteHistoryHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id is missing", async () => {
    await deleteHistoryHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["id"], ctx);
  });

  it("should delete history and set status to 204", async () => {
    ctx.params.id = "history123";

    await deleteHistoryHandler(ctx);

    expect(ctx.status).toBe(204);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "history123";
    deleteHistory.mockImplementation(() => {
      throw error;
    });

    await deleteHistoryHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("deleteAllHistoryByUserHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when kc_id is missing", async () => {
    await deleteAllHistoryByUserHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["kc_id"], ctx);
  });

  it("should delete all history by user and set status to 204", async () => {
    ctx.params.kc_id = "kc123";
    deleteAllHistoryByUserService.mockResolvedValue([]);

    await deleteAllHistoryByUserHandler(ctx);

    expect(ctx.status).toBe(204);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.kc_id = "kc123";
    deleteAllHistoryByUserService.mockImplementation(() => {
      throw error;
    });

    await deleteAllHistoryByUserHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
