// get.test.js

const { getHistoryHandler } = require("@/app/api/user-search-history/get");
const {
  handleMissingParameters,
  handleInternalError,
} = require("@/app/utils/errorHandler");
const { getSearchHistoryByUser } = require("@/app/dal/searchHistoryService");

jest.mock("@/app/utils/errorHandler");
jest.mock("@/app/dal/searchHistoryService");

describe("getHistoryHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      request: {
        body: {},
      },
      body: null,
      status: null,
      params: {},
    };
  });

  it("should handle missing parameters", async () => {
    await getHistoryHandler(ctx);
    expect(handleMissingParameters).toHaveBeenCalledWith(["kc_id"], ctx);
  });

  it("should return search history and set status to 200", async () => {
    const mockHistory = [{ id: 1, search: "test search" }];
    ctx.params = {
      kc_id: "kc123",
    };
    getSearchHistoryByUser.mockResolvedValue(mockHistory);

    await getHistoryHandler(ctx);

    expect(ctx.status).toBe(200);
    expect(ctx.body).toEqual(mockHistory);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params = {
      kc_id: "kc123",
    };
    getSearchHistoryByUser.mockImplementation(() => {
      throw error;
    });

    await getHistoryHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
