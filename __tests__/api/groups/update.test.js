// update.test.js

const {
  updateGroupHandler,
  addUserToGroupHandler,
  removeUserFromGroupHandler,
  addPolicyToGroupHandler,
} = require("@/app/api/groups/update");
const {
  createGroup,
  addUserToGroup,
  removeUserFromGroup,
  addPolicyToGroup,
  updateGroup,
} = require("@/app/dal/groupService");
const {
  handleMissingParameters,
  handleInternalError,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/groupService");
jest.mock("@/app/utils/errorHandler");

describe("updateGroupHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when name, description, or id is missing", async () => {
    await updateGroupHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["name", "description", "id"],
      ctx
    );
  });

  it("should update group and set status to 201", async () => {
    ctx.params.id = "group123";
    ctx.request.body = {
      name: "Updated Group",
      description: "Updated Description",
    };
    const mockResponse = {
      id: "group123",
      name: "Updated Group",
      description: "Updated Description",
    };
    updateGroup.mockResolvedValue(mockResponse);

    await updateGroupHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "group123";
    ctx.request.body = {
      name: "Updated Group",
      description: "Updated Description",
    };
    updateGroup.mockImplementation(() => {
      throw error;
    });

    await updateGroupHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("addUserToGroupHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when kc_id or groupId is missing", async () => {
    await addUserToGroupHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["kc_id", "id"], ctx);
  });

  it("should add user to group and set status to 201", async () => {
    ctx.params.id = "group123";
    ctx.request.body = {
      kc_id: "kc123",
    };
    const mockResponse = { kc_id: "kc123", groupId: "group123" };
    addUserToGroup.mockResolvedValue(mockResponse);

    await addUserToGroupHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(201);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "group123";
    ctx.request.body = {
      kc_id: "kc123",
    };
    addUserToGroup.mockImplementation(() => {
      throw error;
    });

    await addUserToGroupHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("removeUserFromGroupHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when kc_id or id is missing", async () => {
    await removeUserFromGroupHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["kc_id", "id"], ctx);
  });

  it("should remove user from group and set status to 204", async () => {
    ctx.params = { kc_id: "kc123", id: "group123" };
    removeUserFromGroup.mockResolvedValue({});

    await removeUserFromGroupHandler(ctx);

    expect(ctx.body).toEqual(null);
    expect(ctx.status).toBe(204);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params = { kc_id: "kc123", id: "group123" };
    removeUserFromGroup.mockImplementation(() => {
      throw error;
    });

    await removeUserFromGroupHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("addPolicyToGroupHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when policy_id or id is missing", async () => {
    await addPolicyToGroupHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["policy_id", "id"],
      ctx
    );
  });

  it("should add policy to group and set status to 201", async () => {
    ctx.params.id = "group123";
    ctx.request.body = {
      policy_id: "policy123",
    };
    const mockResponse = { policy_id: "policy123", group_id: "group123" };
    addPolicyToGroup.mockResolvedValue(mockResponse);

    await addPolicyToGroupHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(201);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "group123";
    ctx.request.body = {
      policy_id: "policy123",
    };
    addPolicyToGroup.mockImplementation(() => {
      throw error;
    });

    await addPolicyToGroupHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
