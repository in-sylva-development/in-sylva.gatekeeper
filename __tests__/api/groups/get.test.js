const {
  getGroupHandler,
  getAllGroupsHandler,
} = require("@/app/api/groups/get");
const { getAllGroups, getGroup } = require("@/app/dal/groupService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/groupService");
jest.mock("@/app/utils/errorHandler");

describe("getGroupHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id is missing", async () => {
    await getGroupHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["id"], ctx);
  });

  it("should get group and set status to 200", async () => {
    ctx.params.id = "group123";
    const mockResponse = { id: "group123", name: "Test Group" };
    getGroup.mockResolvedValue(mockResponse);

    await getGroupHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "group123";
    getGroup.mockImplementation(() => {
      throw error;
    });

    await getGroupHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("getAllGroupsHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      body: null,
      status: null,
    };
  });

  it("should get all groups and set status to 200", async () => {
    const mockResponse = [
      { id: 1, name: "Group 1" },
      { id: 2, name: "Group 2" },
    ];
    getAllGroups.mockResolvedValue(mockResponse);

    await getAllGroupsHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    getAllGroups.mockImplementation(() => {
      throw error;
    });

    await getAllGroupsHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
