const { createGroupHandler } = require("@/app/api/groups/post");
const { createGroup } = require("@/app/dal/groupService");
const {
  handleMissingParameters,
  handleInternalError,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/groupService");
jest.mock("@/app/utils/errorHandler");

describe("createGroupHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when name, description, or kc_id is missing", async () => {
    await createGroupHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["name", "description", "kc_id"],
      ctx
    );
  });

  it("should create group and set status to 201", async () => {
    ctx.request.body = {
      name: "Test Group",
      description: "Test Description",
      kc_id: "kc123",
    };
    const mockResponse = {
      id: 1,
      name: "Test Group",
      description: "Test Description",
    };
    createGroup.mockResolvedValue(mockResponse);

    await createGroupHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(201);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.request.body = {
      name: "Test Group",
      description: "Test Description",
      kc_id: "kc123",
    };
    createGroup.mockImplementation(() => {
      throw error;
    });

    await createGroupHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
