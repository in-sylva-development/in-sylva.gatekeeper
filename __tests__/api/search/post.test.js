const { getQueryResultsHandler } = require("@/app/api/search/post");
const { search } = require("@/app/dal/elasticService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/elasticService");
jest.mock("@/app/utils/errorHandler");

describe("getQueryResultsHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when query is missing", async () => {
    await getQueryResultsHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["query", "sourcesId", "fieldsId"],
      ctx
    );
  });

  it("should get query results and set status to 200", async () => {
    ctx.request.body = {
      query: "test query",
      sourcesId: [1, 2, 3],
      fieldsId: [1, 2, 3],
      index: "test_index",
      scroll_id: "test_scroll_id",
    };
    const mockResponse = { hits: { hits: [] } };
    search.mockResolvedValue(mockResponse);

    await getQueryResultsHandler(ctx);

    expect(ctx.body).toEqual([]);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.request.body = {
      query: "test query",
      sourcesId: [1, 2, 3],
      fieldsId: [1, 2, 3],
      index: "test_index",
      scroll_id: "test_scroll_id",
    };
    search.mockImplementation(() => {
      throw error;
    });

    await getQueryResultsHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
