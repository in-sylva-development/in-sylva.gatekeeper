// post.test.js

const {
  createRequestHandler,
  updateRequestHandler,
} = require("@/app/api/user-requests/post");
const {
  createRequest,
  updateRequest,
} = require("@/app/dal/userRequestService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/userRequestService");
jest.mock("@/app/utils/errorHandler");

describe("createRequestHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when kc_id or message is missing", async () => {
    await createRequestHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["kc_id", "message"],
      ctx
    );
  });

  it("should create request and set status to 201", async () => {
    ctx.params.kc_id = "kc123";
    ctx.request.body = { message: "test message" };
    const mockResponse = { id: 1, kc_id: "kc123", message: "test message" };
    createRequest.mockResolvedValue(mockResponse);

    await createRequestHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(201);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.kc_id = "kc123";
    ctx.request.body = { message: "test message" };
    createRequest.mockImplementation(() => {
      throw error;
    });

    await createRequestHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("updateRequestHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      request: {
        body: {},
      },
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id or is_processed is missing", async () => {
    await updateRequestHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["id", "is_processed"],
      ctx
    );
  });

  it("should return 400 if is_processed is not a boolean", async () => {
    ctx.params.id = {
      id: "req123",
    };
    ctx.request.body.is_processed = "not_boolean";

    jest.mock("@/app/utils/errorHandler", () => {
      const originalModule = jest.requireActual("@/app/utils/errorHandler");
      return {
        ...originalModule,
        handleInternalError: jest.fn(),
      };
    });

    await updateRequestHandler(ctx);
    expect(handleMissingParameters.mock.calls).toHaveLength(1);
    expect(handleMissingParameters).toHaveBeenCalledWith(
      ["is_processed must be a boolean"],
      ctx
    );
  });

  it("should update request and set status to 201", async () => {
    ctx.params.id = "req123";
    ctx.request.body.is_processed = true;
    const mockResponse = { id: "req123", is_processed: "true" };
    updateRequest.mockResolvedValue(mockResponse);

    await updateRequestHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(201);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "req123";
    ctx.request.body.is_processed = true;
    updateRequest.mockImplementation(() => {
      throw error;
    });

    await updateRequestHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
