// delete.test.js

const { deleteRequestHandler } = require("@/app/api/user-requests/delete");
const { deleteRequest } = require("@/app/dal/userRequestService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/userRequestService");
jest.mock("@/app/utils/errorHandler");

describe("deleteRequestHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      params: {},
      body: null,
      status: null,
    };
  });

  it("should handle missing parameters when id is missing", async () => {
    await deleteRequestHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["id"], ctx);
  });

  it("should delete request and set status to 204", async () => {
    ctx.params.id = "req123";
    deleteRequest.mockResolvedValue({});

    await deleteRequestHandler(ctx);

    expect(ctx.body).toEqual(null);
    expect(ctx.status).toBe(204);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    ctx.params.id = "req123";
    deleteRequest.mockImplementation(() => {
      throw error;
    });

    await deleteRequestHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
