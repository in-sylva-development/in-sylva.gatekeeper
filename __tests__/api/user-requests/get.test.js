// get.test.js

const {
  getAllRequestsHandler,
  getAllPendingRequestsHandler,
  getAllUserRequestsHandler,
  getAllPendingUserRequestsHandler,
} = require("@/app/api/user-requests/get");
const {
  getAllRequests,
  getAllPendingRequests,
} = require("@/app/dal/userRequestService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

jest.mock("@/app/dal/userRequestService");
jest.mock("@/app/utils/errorHandler");

describe("getAllRequestsHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      body: null,
      status: null,
    };
  });

  it("should get all requests and set status to 200", async () => {
    const mockResponse = [{ id: 1, request: "test request" }];
    getAllRequests.mockResolvedValue(mockResponse);

    await getAllRequestsHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    getAllRequests.mockImplementation(() => {
      throw error;
    });

    await getAllRequestsHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("getAllPendingRequestsHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      body: null,
      status: null,
    };
  });

  it("should get all pending requests and set status to 200", async () => {
    const mockResponse = [{ id: 1, request: "pending request" }];
    getAllPendingRequests.mockResolvedValue(mockResponse);

    await getAllPendingRequestsHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    getAllPendingRequests.mockImplementation(() => {
      throw error;
    });

    await getAllPendingRequestsHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("getAllUserRequestsHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      body: null,
      status: null,
      params: { kc_id: "test-id" },
    };
  });

  it("should get all requests for a user and set status to 200", async () => {
    const mockResponse = [{ id: 1, request: "test request" }];
    getAllRequests.mockResolvedValue(mockResponse);

    await getAllUserRequestsHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle missing parameters", async () => {
    ctx.params.kc_id = null;

    await getAllUserRequestsHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["kc_id"], ctx);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    getAllRequests.mockImplementation(() => {
      throw error;
    });

    await getAllUserRequestsHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});

describe("getAllPendingUserRequestsHandler", () => {
  let ctx;

  beforeEach(() => {
    ctx = {
      body: null,
      status: null,
      params: { kc_id: "test-id" },
    };
  });

  it("should get all pending requests for a user and set status to 200", async () => {
    const mockResponse = [{ id: 1, request: "pending request" }];
    getAllPendingRequests.mockResolvedValue(mockResponse);

    await getAllPendingUserRequestsHandler(ctx);

    expect(ctx.body).toEqual(mockResponse);
    expect(ctx.status).toBe(200);
  });

  it("should handle missing parameters", async () => {
    ctx.params.kc_id = null;

    await getAllPendingUserRequestsHandler(ctx);

    expect(handleMissingParameters).toHaveBeenCalledWith(["kc_id"], ctx);
  });

  it("should handle internal error", async () => {
    const error = new Error("Internal Error");
    getAllPendingRequests.mockImplementation(() => {
      throw error;
    });

    await getAllPendingUserRequestsHandler(ctx);

    expect(handleInternalError).toHaveBeenCalledWith(error, ctx);
  });
});
