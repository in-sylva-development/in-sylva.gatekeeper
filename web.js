"use strict";

const { promisify } = require("util");
const http = require("http");
const logger = require("@/app/utils/format");
const app = require("@/server");
require("@/app/config/server");
require("@/app/config/postgres");
require("@/app/config/elk");
require("@/app/config/mongo");

logger.level = process.env.LOGGER_LEVEL || "info";

try {
  const server = http.createServer(app.callback());
  const serverListen = promisify(server.listen).bind(server);

  serverListen(process.env.PORT)
    .then(() => {
      logger.info(
        `in-sylva.gatekeeper service is up and running on localhost:${process.env.PORT}`
      );
    })
    .catch((err) => {
      logger.error(err);
      process.exit(1);
    });
} catch (error) {
  console.error("Failed to start app:", error);
  process.exit(1);
}
