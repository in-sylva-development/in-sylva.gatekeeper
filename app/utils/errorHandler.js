const util = require("util");
const logger = require("../utils/format");

async function handleInternalError(err, ctx) {
  const error = JSON.stringify(
    util.inspect(err, { compact: false, depth: 1, breakLength: 80 })
  );
  ctx.body = {
    error: "Internal server error",
    statusCode: 500,
    message: err.message,
  };
  ctx.status = 500;
  logger.error(error);
}

async function handleMissingParameters(missingParameters, ctx) {
  ctx.status = 400;
  ctx.body = {
    error: "Bad request",
    statusCode: 400,
    message: `The following parameters are missing: ${missingParameters.join(
      ", "
    )}`,
  };
}

module.exports = {
  handleInternalError,
  handleMissingParameters,
};
