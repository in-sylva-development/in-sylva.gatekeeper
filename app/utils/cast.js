function isBooleanString(value) {
  if (typeof value !== "string") {
    return false;
  }
  return value.toLowerCase() === "true" || value.toLowerCase() === "false";
}

function castStringToBoolean(value) {
  if (isBooleanString(value)) {
    return value.toLowerCase() === "true";
  }
  return value;
}

module.exports = {
  isBooleanString,
  castStringToBoolean,
};
