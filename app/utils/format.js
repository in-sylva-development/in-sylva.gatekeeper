'use strict'

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf, colorize, splat, label } = format;
const path = require('path');


const mFormat = printf((info) => {
    if (info.meta && info.meta instanceof Error) {
        return `${info.timestamp} ${info.level} ${info.message} : ${info.meta.stack}`;
    }
    return `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`;
});

const LOG_LEVEL = process.env.LOGGER_LEVEL || 'debug';
const logger = createLogger({
    transports: [
        new (transports.Console)(
            {
                level: LOG_LEVEL,
                format: combine(
                    label({ label: path.basename(process.mainModule.filename) }),
                    colorize(),
                    timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
                    splat(),
                    mFormat
                )
            }
        )
    ]
});
module.exports = logger;



