"use strict";

const prisma = require("@/prisma/client");

async function createRequest(kc_id, message) {
  if (!kc_id || !message) {
    throw Error("The request body is empty!");
  }

  const user = await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
  });

  if (!user) {
    throw Error("The user account could not found.");
  }

  const result = await prisma.userRequest.create({
    data: {
      user: {
        connect: {
          id: user.id,
        },
      },
      request_message: message,
      is_processed: false,
    },
  });
  return result;
}

async function getAllRequests() {
  return await prisma.userRequest.findMany();
}

async function getAllPendingRequests() {
  const requests = await prisma.userRequest.findMany({
    where: {
      is_processed: false,
    },
  });

  return requests;
}

async function updateRequest(id, isProcessed) {
  if (!id || isProcessed === undefined) {
    throw Error("The request body is empty!");
  }

  const request = await prisma.userRequest.findFirst({
    where: {
      id: +id,
    },
  });
  if (!request) {
    throw Error("The request could not found.");
  }

  const result = await prisma.userRequest.update({
    where: {
      id: +id,
    },
    data: {
      is_processed: isProcessed,
    },
  });

  return result;
}

async function getUserRequests(kc_id) {
  if (!kc_id) {
    throw Error("The request body is empty!");
  }

  const user = await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
  });

  if (!user) {
    throw Error("The user account could not found.");
  }

  const requests = await prisma.userRequest.findMany({
    where: {
      user_id: user.id,
    },
  });

  return requests;
}

async function getPendingRequests(kc_id) {
  if (!kc_id) {
    throw Error("The request body is empty!");
  }

  const user = await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
  });

  if (!user) {
    throw Error("The user account could not found.");
  }

  const requests = await prisma.userRequest.findMany({
    where: {
      user_id: user.id,
      is_processed: false,
    },
  });

  return requests;
}

async function deleteRequest(id) {
  if (!id) {
    throw Error("The request body is empty!");
  }

  const result = await prisma.userRequest.delete({
    where: {
      id: +id,
    },
  });

  return result;
}

module.exports = {
  createRequest,
  getAllRequests,
  getUserRequests,
  getAllPendingRequests,
  getPendingRequests,
  updateRequest,
  deleteRequest,
};
