"use strict";
const prisma = require("../../prisma/client");

async function createGroup(name, description, kc_id) {
  if (!name || !description || !kc_id) {
    throw Error("The request body is empty!");
  }
  const user = await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
  });

  if (!user) {
    throw Error("The user account could not be found.");
  }

  const group = await prisma.group.findFirst({
    where: {
      name: name,
    },
  });
  if (group) {
    throw Error(`A group named '${group.name}' already exists.`);
  }

  const newGroup = await prisma.group.create({
    data: {
      name: name,
      description: description,
      admin: {
        connect: {
          id: user.id,
        },
      },
    },
  });

  return newGroup;
}

async function addPolicyToGroup(policyId, groupId) {
  if (!groupId || !policyId) {
    throw Error("The request body is empty!");
  }

  const group = await prisma.group.findFirst({
    where: {
      id: +groupId,
    },
  });

  if (!group) {
    throw Error("The group could not be found.");
  }

  const policy = await prisma.policy.findFirst({
    where: {
      id: +policyId,
    },
  });

  if (!policy) {
    throw Error("The policy could not be found.");
  }

  const groupPolicy = await prisma.group.findFirst({
    where: {
      id: +groupId,
      policies: {
        some: {
          policy_id: +policyId,
        },
      },
    },
  });

  if (groupPolicy) {
    throw Error("The policy is already assigned to the group.");
  }

  const newGroupPolicy = await prisma.groupPolicy.create({
    data: {
      group_id: +groupId,
      policy_id: +policyId,
    },
  });

  return newGroupPolicy;
}

async function updateGroup(id, name, description) {
  if (!name || !description || !id) {
    throw Error("The request body is empty!");
  }

  const group = await prisma.group.findFirst({
    where: {
      id: +id,
    },
  });

  if (!group) {
    throw Error("The group could not be found.");
  }

  const updated = await prisma.group.update({
    where: {
      id: +id,
    },
    data: {
      name: name,
      description: description,
    },
  });

  return updated;
}

async function deleteGroup(id) {
  if (!id) {
    throw Error("The request body is empty!");
  }
  const group = await prisma.group.findFirst({
    where: {
      id: +id,
    },
  });

  if (!group) {
    throw Error("The group could not be found.");
  }
  const deleted = await prisma.group.delete({
    where: {
      id: +id,
    },
  });

  return deleted;
}

async function removePolicyFromGroup(policyId, groupId) {
  if (!groupId || !policyId) {
    throw Error("The request body is empty!");
  }

  const policy = await prisma.policy.findFirst({
    where: {
      id: +policyId,
    },
  });

  if (!policy) {
    throw Error("The policy could not be found.");
  }

  const group = await prisma.group.findFirst({
    where: {
      id: +groupId,
    },
  });

  if (!group) {
    throw Error("The group could not be found.");
  }

  const groupPolicy = await prisma.groupPolicy.delete({
    where: {
      group_id_policy_id: {
        group_id: +groupId,
        policy_id: +policyId,
      },
    },
  });

  return groupPolicy;
}

async function addUserToGroup(kc_id, groupId) {
  if (!kc_id || !groupId) {
    throw Error("The request body is empty!");
  }

  const user = await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
  });
  if (!user) {
    throw Error("The user account could not be found.");
  }

  const group = await prisma.group.findFirst({
    where: {
      id: +groupId,
    },
  });

  if (!group) {
    throw Error(
      "The group could not be found. Please ensure the group exists."
    );
  }
  const newGroupUser = await prisma.groupUser.create({
    data: {
      group_id: +groupId,
      user_id: user.id,
    },
  });
  return newGroupUser;
}

async function removeUserFromGroup(kc_id, groupId) {
  if (!groupId || !kc_id) {
    throw Error("The request body is empty!");
  }

  const user = await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
  });
  if (!user) {
    throw Error("The user account could not be found.");
  }

  const group = await prisma.group.findFirst({
    where: {
      id: +groupId,
    },
  });

  if (!group) {
    throw Error("The group could not be found.");
  }

  const groupUser = await prisma.groupUser.delete({
    where: {
      group_id_user_id: {
        group_id: +groupId,
        user_id: user.id,
      },
    },
  });

  return groupUser;
}

async function getGroup(id) {
  if (!id) {
    throw Error("The request body is empty!");
  }
  return await prisma.group.findFirst({
    where: {
      id: +id,
    },
    include: {
      users: {
        include: {
          user: true,
        },
      },
      policies: {
        include: {
          policy: {
            include: {
              std_fields: {
                include: {
                  std_field: true,
                },
              },
              sources: {
                include: {
                  source: true,
                },
              },
            },
          },
        },
      },
    },
  });
}

async function getAllGroups() {
  return await prisma.group.findMany({
    include: {
      users: {
        include: {
          user: true,
        },
      },
      policies: {
        include: {
          policy: true,
        },
      },
    },
  });
}

module.exports = {
  createGroup,
  addPolicyToGroup,
  addUserToGroup,
  updateGroup,
  deleteGroup,
  removePolicyFromGroup,
  removeUserFromGroup,
  getAllGroups,
  getGroup,
};
