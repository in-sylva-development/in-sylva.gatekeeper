"use strict";
const prisma = require("../../prisma/client");
const logger = require("@/app/utils/format");

async function getStdFields() {
    const stdFields = await prisma.stdField.findMany({
        include: {
            policies: {
                include: {
                    policy: {
                        include: {
                            sources: {
                                include: {
                                    source: true,
                                },
                            },
                        },
                    },
                },
            },
        },
    });
    return stdFields;
}

async function getPublicStdFields() {
    const stdFields = await prisma.stdField.findMany({
        where: {
            ispublic: true,
        },
        include: {
            policies: {
                include: {
                    policy: {
                        include: {
                            sources: {
                                include: {
                                    source: true,
                                },
                            },
                        },
                    },
                },
            },
        },
    });
    return stdFields;
}

async function getStdField(id) {
    if (!id) {
        throw Error("The request body is empty!");
    }

    const stdField = await prisma.stdField.findUnique({
        where: {
            id: +id,
        },
    });

    if (!stdField) {
        throw Error(
            `The stdField with id ${id} could not be found in the database!`
        );
    }

    return stdField;
}

async function createOrUpdateStdField({
                                          id,
                                          category,
                                          field_name,
                                          definition_and_comment,
                                          obligation_or_condition,
                                          cardinality,
                                          field_type,
                                          values,
                                          ispublic,
                                          isoptional,
                                          list_url, default_display_fields
                                      }) {
    if (!field_name) {
        throw Error("The request body is empty!");
    }

    logger.debug(default_display_fields);
    // Transform string to bool
    const is_default_display_fields = default_display_fields === "1" ? true : false;

    if (id) {
        const stdField = await prisma.stdField.findFirst({
            where: {
                id: +id,
            },
        });

        if (!stdField) {
            throw Error(`The stdField with id '${id} doesn't exist`);
        }

        if (!typeof ispublic === "boolean" || !typeof isoptional === "boolean") {
            throw Error("ispublic and isoptional must be boolean");
        }

        return await prisma.stdField.update({
            where: {
                id: +id,
            },
            update: {
                category,
                definition_and_comment,
                obligation_or_condition,
                cardinality,
                field_type,
                values,
                ispublic,
                isoptional,
                list_url,
                default_display_fields: is_default_display_fields
            },
        });
    } else {
        // Need to create the std_field
        const stdField = await prisma.stdField.findFirst({
            where: {
                field_name: field_name,
            },
        });

        if (stdField) {
            throw Error(
                `The stdField named '${field_name} already inserted to database!`
            );
        }

        const newStdField = await prisma.stdField.create({
            data: {
                category,
                field_name,
                definition_and_comment,
                obligation_or_condition,
                cardinality,
                field_type,
                values,
                ispublic,
                isoptional,
                list_url,
                default_display_fields: is_default_display_fields
            },
        });
        return newStdField;
    }
}

async function deleteAllStdFields() {
    return await prisma.stdField.deleteMany();
}

module.exports = {
    getStdField,
    getStdFields,
    getPublicStdFields,
    createOrUpdateStdField,
    deleteAllStdFields,
};
