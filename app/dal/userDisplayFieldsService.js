"use strict";

const prisma = require("@/prisma/client");
const logger = require("../utils/format");

async function getUserDisplayFields(kc_id) {
  return await prisma.stdField.findMany({
    where: {
      display_fields: {
        some: {
          user: {
            kc_id: kc_id,
          },
        },
      },
    },
  });
}

async function setUserDisplayFields(kc_id, fields_ids) {
  const user = await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
  });

  if (!user) {
    throw new Error("User not found");
  }

  let ids = [];
  // Reset the user display fields
  try {
    await prisma.displayStdField.deleteMany({
      where: {
        user_id: user.id,
      },
    });
  } catch (err) {
    logger.error("Error while resetting user display fields", {
      error: err.message,
    });
  }
  for (const field_id of fields_ids) {
    try {
      const id = await prisma.displayStdField.create({
        data: {
          user_id: user.id,
          std_field_id: +field_id,
        },
      });
      ids.push(id);
    } catch (err) {
      logger.error("Error while adding user display field", {
        error: err.message,
      });
    }
  }
  return ids;
}

module.exports = {
  getUserDisplayFields,
  setUserDisplayFields,
  setUserDisplayFields,
};
