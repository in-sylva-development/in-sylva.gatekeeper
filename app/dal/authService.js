const logger = require("@/app/utils/format");

async function isAccessTokenValid(accessToken) {
  if (!accessToken) {
    return false;
  }

  const issuerUrl = process.env.KEYCLOAK_BASE_URL;
  const realm = process.env.KEYCLOAK_REALM;
  const userEndpoint =
    issuerUrl + "/realms/" + realm + "/protocol/openid-connect/userinfo";

  const response = await fetch(userEndpoint, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
  switch (response.status) {
    case 401:
      return false;
    case 200:
      return true;
    default:
      logger.error(`Error validating access token: ${response.statusText}`);
      return false;
  }
}

module.exports = {
  isAccessTokenValid,
};
