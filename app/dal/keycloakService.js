"use strict";

const KcAdminClient = require("keycloak-admin").default;

const keycloakAdmin = new KcAdminClient({
  baseUrl: process.env.KEYCLOAK_BASE_URL,
  realmName: "master",
});

async function auth() {
  await keycloakAdmin.auth({
    username: process.env.KEYCLOAK_USER,
    password: process.env.KEYCLOAK_PASSWORD,
    grantType: "password",
    clientId: "admin-cli",
  });
}

async function getAllKeycloakUsers() {
  try {
    await auth();
    const users = await keycloakAdmin.users.find({
      realm: process.env.KEYCLOAK_REALM,
    });

    return users;
  } catch (err) {
    throw Error(err);
  }
}

async function deleteKeycloakUser({ kc_id }) {
  try {
    if (!kc_id) {
      throw Error("The kc_id is missing!");
    }

    await auth();

    return await keycloakAdmin.users.del({
      id: kc_id,
      realm: process.env.KEYCLOAK_REALM,
    });
  } catch (error) {
    throw Error(error);
  }
}

module.exports = {
  getAllKeycloakUsers,
  deleteKeycloakUser,
};