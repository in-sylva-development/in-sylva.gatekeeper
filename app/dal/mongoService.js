const { mongoClient, dbName } = require("@/app/config/mongo");
const logger = require("../utils/format");
const { ObjectId } = require("mongodb");

async function addSourceToMongoDB(metadata) {
  await mongoClient.connect();
  const db = mongoClient.db(dbName);

  const result = await db.collection("metaurfm").insertOne(metadata);
  return result;
}

async function removeSourceFromMongoDB(id) {
  logger.debug(`Removing source from mongo with id: ${id}`);
  await mongoClient.connect();
  const db = mongoClient.db(dbName);

  const result = await db
    .collection("metaurfm")
    .deleteOne({ _id: new ObjectId(id) });
  if (result.deletedCount === 0) {
    logger.error(`Source with id: ${id} not found`);
    return false;
  }
  logger.debug(`Source with id: ${id} removed`);
  return true;
}

module.exports = {
  addSourceToMongoDB,
  removeSourceFromMongoDB,
};
