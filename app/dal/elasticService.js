const {Client} = require("@elastic/elasticsearch");
const prisma = require("@/prisma/client");
const {configElk} = require("@/app/config/elk");
const logger = require("@/app/utils/format");

async function getClientInstance() {
    logger.debug(`Connecting to ${configElk.ELK_URL}`);
    const client = new Client({
        node: configElk.ELK_URL,
        maxRetries: 5,
        requestTimeout: 60000,
        sniffOnStart: false,
        auth: {
            username: configElk.ELK_USERNAME,
            password: configElk.ELK_PASSWORD,
        },
    });
    if (!(await client.ping())) {
        logger.error("Failed to connect to ElasticSearch");
    }
    logger.debug("Connected to ElasticSearch");
    return client;
}

async function prepareForBulk(metaUrfms, elkIndex) {
    const client = await getClientInstance();
    const response = await client.indices.create(
        {
            index: elkIndex,
        },
        {ignore: [400]}
    );

    if (!response.acknowledged) {
        logger.error(`Index ${elkIndex} not created`);
        throw new Error(`Index ${elkIndex} not created`);
    }
    logger.debug(`Index ${elkIndex} created`);

    const body = metaUrfms.flatMap((doc) => [
        {index: {_index: elkIndex}},
        doc,
    ]);
    return body;
}

async function bulk(body) {
    const client = await getClientInstance();
    logger.debug("Indexing documents");
    const {errors, items} = await client.bulk({body, refresh: true});
    if (errors) {
        logger.error("Error while bulk indexing", errors);
        return false;
    }
    logger.debug("Documents indexed :" + items.length);
    return true;
}

async function search({query, sourcesId, scroll_id, advancedQuery}) {
    const client = await getClientInstance();
    logger.debug(`Elasticsearch query: ${JSON.stringify(query)}`);
    logger.debug("Advanced query: " + advancedQuery);
    if (scroll_id) {
        const {body} = await client.scroll({
            index,
            scroll_id,
            scroll: "10s",
        });
        return body;
    } else {
        let queryBuilder;
        if (!advancedQuery) {
            queryBuilder = {
                query: {
                    query_string: {
                        query: query,
                        default_operator: "or",
                    },
                },
            };
        } else {
            try {
                // remove trailing and leading whitespaces, tabs, newlines, commas, and semicolons
                query = query.replace(/(^[,\s\n\t]+)|([,\s\n\t]+$)/g, "");
                logger.debug("Advanced query: " + query);
                query = JSON.parse(query);
                queryBuilder = {
                    query: query
                };
            } catch (error) {
                logger.error("Invalid query", error);
                return {hits: {hits: []}};
            }
        }
        let indices = [];
        for (let sourceId of sourcesId) {
            const source = await prisma.source.findUnique({
                where: {
                    id: +sourceId,
                },
                include: {
                    source_indices: true,
                },
            });
            if (!source) {
                logger.error(`Source ${sourceId} not found`);
                continue;
            }
            for (let index of source.source_indices) {
                indices.push(index.index_id);
            }
        }
        logger.debug(`Searching in indices: ${indices.join(",")}`);
        if (indices.length === 0) {
            logger.error("No indices found for the given sources");
            return {hits: {hits: []}};
        }
        logger.debug("Query : " + JSON.stringify(queryBuilder));
        const response = await client.search({
            index: indices.join(","),
            body: queryBuilder,
            scroll: "10s",
        });
        logger.debug(`Search results: ${response.hits.total.value}`);
        return response;
    }
}

async function countByIndex(index) {
    const client = await getClientInstance();
    const {body: count} = await client.count({index});
    return count;
}

async function deleteIndex(index) {
    const client = await getClientInstance();
    logger.debug(`Deleting index ${index}`);
    try {
        await client.indices.delete({index});
    } catch (error) {
        logger.error(`Error while deleting index ${index}`, error);
        return false;
    }
    return true;
}

module.exports = {
    prepareForBulk,
    bulk,
    countByIndex,
    deleteIndex,
    search,
};
