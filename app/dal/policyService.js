"use strict";

const prisma = require("@/prisma/client");

async function createPolicy(name, kc_id) {
  if (!name || !kc_id) {
    throw Error("The request body is empty!");
  }

  const user = await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
  });

  if (!user) {
    throw Error("The user account could not be found.");
  }

  const policy = await prisma.policy.findFirst({
    where: {
      name: name,
    },
  });

  if (policy) {
    throw Error(`The policy named '${name} already inserted to database!`);
  }

  const newPolicy = await prisma.policy.create({
    data: {
      name: name,
      user: {
        connect: {
          id: user.id,
        },
      },
      is_default: false,
    },
  });

  return newPolicy;
}

async function addFieldToPolicy(stdFieldId, policyId) {
  if (!policyId || !stdFieldId) {
    throw Error("The request body is empty!");
  }

  const policy = await prisma.policy.findFirst({
    where: {
      id: +policyId,
    },
  });

  if (!policy) {
    throw Error(
      `This policyId:${policyId} could not be found in the database!`
    );
  }

  const stdField = await prisma.stdField.findFirst({
    where: {
      id: +stdFieldId,
    },
  });
  if (!stdField) {
    throw Error(
      `This stdFieldId:${stdFieldId} could not be found in the database!`
    );
  }

  const stdFieldPolicy = await prisma.policy.findFirst({
    where: {
      id: +policyId,
      std_fields: {
        some: {
          std_field_id: +stdFieldId,
        },
      },
    },
  });

  if (stdFieldPolicy) {
    throw Error(`This stdFieldId:${stdFieldId} already exists in the policy!`);
  }

  const newPolicyField = await prisma.policyStdField.create({
    data: {
      policy: {
        connect: {
          id: +policyId,
        },
      },
      std_field: {
        connect: {
          id: +stdFieldId,
        },
      },
    },
  });

  return newPolicyField;
}

async function addSourceToPolicy(sourceId, policyId) {
  if (!policyId || !sourceId) {
    throw Error("The request body is empty!");
  }

  const policy = await prisma.policy.findFirst({
    where: {
      id: +policyId,
    },
  });

  if (!policy) {
    throw Error(
      `This policyId:${policyId} could not be found in the database!`
    );
  }

  const source = await prisma.source.findFirst({
    where: {
      id: +sourceId,
    },
  });
  if (!source) {
    throw Error(
      `This sourceId:${sourceId} could not be found in the database!`
    );
  }

  const sourcePolicy = await prisma.policy.findFirst({
    where: {
      id: +policyId,
      sources: {
        some: {
          source_id: +sourceId,
        },
      },
    },
  });

  if (sourcePolicy) {
    throw Error(`This sourceId:${sourceId} already exists in the policy!`);
  }

  const newPolicySource = await prisma.policySource.create({
    data: {
      policy_id: +policyId,
      source_id: +sourceId,
    },
  });
  return newPolicySource;
}

async function removeSourceFromPolicy(sourceId, policyId) {
  if (!policyId || !sourceId) {
    throw Error("The request body is empty!");
  }

  const policy = await prisma.policy.findFirst({
    where: {
      id: +policyId,
    },
  });

  if (!policy) {
    throw Error(
      `This policyId:${policyId} could not be found in the database!`
    );
  }

  const source = await prisma.source.findFirst({
    where: {
      id: +sourceId,
    },
  });

  if (!source) {
    throw Error(
      `This sourceId:${sourceId} could not be found in the database!`
    );
  }

  const sourcePolicy = await prisma.policy.findFirst({
    where: {
      id: +policyId,
      sources: {
        some: {
          source_id: +sourceId,
        },
      },
    },
  });

  if (!sourcePolicy) {
    throw Error(`This sourceId:${sourceId} does not exist in the policy!`);
  }

  const deletedPolicySource = await prisma.policySource.delete({
    where: {
      policy_id_source_id: {
        policy_id: +policyId,
        source_id: +sourceId,
      },
    },
  });

  return deletedPolicySource;
}

async function updatePolicy(id, name, isDefault) {
  if (!id || !name || !isDefault) {
    throw Error("The request body is empty!");
  }

  const policy = await prisma.policy.update({
    where: {
      id: id,
    },
    data: {
      name: name,
      is_default: isDefault,
    },
  });

  return policy;
}

async function removeFieldFromPolicy(stdFieldId, policyId) {
  if (!policyId || !stdFieldId) {
    throw Error("The request body is empty!");
  }

  const policy = await prisma.policy.findFirst({
    where: {
      id: +policyId,
    },
  });

  if (!policy) {
    throw Error(
      `This policyId:${policyId} could not be found in the database!`
    );
  }

  const stdField = await prisma.stdField.findFirst({
    where: {
      id: +stdFieldId,
    },
  });

  if (!stdField) {
    throw Error(
      `This stdFieldId:${stdFieldId} could not be found in the database!`
    );
  }

  const stdFieldPolicy = await prisma.policy.findFirst({
    where: {
      id: +policyId,
      std_fields: {
        some: {
          std_field_id: +stdFieldId,
        },
      },
    },
  });

  if (!stdFieldPolicy) {
    throw Error(`This stdFieldId:${stdFieldId} does not exist in the policy!`);
  }

  const deletedPolicyField = await prisma.policyStdField.delete({
    where: {
      policy_id_std_field_id: {
        policy_id: +policyId,
        std_field_id: +stdFieldId,
      },
    },
  });

  return deletedPolicyField;
}

async function deletePolicy(id) {
  if (!id) {
    throw Error("The request body is empty!");
  }

  const policy = await prisma.policy.findFirst({
    where: {
      id: +id,
    },
  });

  if (!policy) {
    throw Error(`This policyId:${id} could
            not be found in the database!`);
  }

  const deletedPolicy = await prisma.policy.delete({
    where: {
      id: +id,
    },
  });

  return deletedPolicy;
}

async function getAllPolicies() {
  return await prisma.policy.findMany({
    include: {
      std_fields: {
        include: {
          std_field: true,
        },
      },
      sources: {
        include: {
          source: true,
        },
      },
      groups: {
        include: {
          group: {
            include: {
              policies: {
                include: {
                  policy: {
                    include: {
                      std_fields: {
                        include: {
                          std_field: true,
                        },
                      },
                      sources: {
                        include: {
                          source: true,
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  });
}

async function getPolicy(policyId) {
  if (!policyId) {
    throw Error("The request body is empty!");
  }

  const assignedPolicies = await prisma.policy.findFirst({
    where: {
      id: policyId,
    },
    include: {
      std_fields: {
        include: {
          std_field: true,
        },
      },
      sources: {
        include: {
          source: true,
        },
      },
      groups: {
        include: {
          group: {
            include: {
              policies: {
                include: {
                  policy: {
                    include: {
                      std_fields: {
                        include: {
                          std_field: true,
                        },
                      },
                      sources: {
                        include: {
                          source: true,
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  });

  return assignedPolicies;
}

module.exports = {
  createPolicy,
  addFieldToPolicy,
  addSourceToPolicy,
  updatePolicy,
  removeFieldFromPolicy,
  removeSourceFromPolicy,
  deletePolicy,
  getAllPolicies,
  getPolicy,
};
