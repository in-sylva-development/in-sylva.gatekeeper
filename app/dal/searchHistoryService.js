"use strict";

const prisma = require("../../prisma/client");

async function addSearchHistoryToUser(
  kc_id,
  query,
  name,
  ui_structure,
  description
) {
  if (!query || !name || !ui_structure || !description) {
    throw Error("The request body is empty!");
  }

  const user = await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
  });
  if (!user) {
    throw Error("The user account could not be found.");
  }

  const result = prisma.user.update({
    where: {
      kc_id: kc_id,
    },
    data: {
      history: {
        create: {
          query: query,
          name: name,
          ui_structure: ui_structure,
          description: description,
        },
      },
    },
    include: {
      history: true,
    },
  });

  return result;
}

async function getSearchHistoryByUser(kc_id) {
  if (!kc_id) {
    throw Error("The request body is empty!");
  }

  const user = await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
  });
  if (!user) {
    throw Error("The user account could not be found.");
  }
  const result = await prisma.searchHistory.findMany({
    where: {
      user_id: user.id,
    },
  });

  return result;
}

async function deleteHistory(id) {
  if (!id) {
    throw Error("The request body is empty!");
  }

  const history = await prisma.searchHistory.findFirst({
    where: {
      id: +id,
    },
  });

  if (!history) {
    throw Error("The history could not be found.");
  }

  return await prisma.searchHistory.delete({
    where: {
      id: +id,
    },
  });
}

async function deleteAllHistoryByUser(kc_id) {
  if (!kc_id) {
    throw Error("The request body is empty!");
  }

  const user = await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
  });
  if (!user) {
    throw Error("The user account could not be found.");
  }

  return await prisma.searchHistory.deleteMany({
    where: {
      user_id: user.id,
    },
  });
}

module.exports = {
  addSearchHistoryToUser,
  deleteHistory,
  getSearchHistoryByUser,
  deleteAllHistoryByUser,
};
