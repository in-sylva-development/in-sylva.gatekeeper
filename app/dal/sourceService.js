"use strict";
const prisma = require("@/prisma/client");
const {
  addSourceToMongoDB,
  removeSourceFromMongoDB,
} = require("@/app/dal/mongoService");
const {
  prepareForBulk,
  bulk,
  countByIndex,
  deleteIndex,
} = require("@/app/dal/elasticService");
const logger = require("@/app/utils/format");

async function getSources() {
  return await prisma.source.findMany({
    include: {
      source_indices: true,
      providers: {
        include: {
          user: true,
        },
      },
    },
  });
}

async function getSource(id) {
  if (!id) {
    throw new Error("Missing required parameter: id");
  }

  return await prisma.source.findUnique({
    where: {
      id,
    },
    include: {
      source_indices: true,
      providers: {
        include: {
          user: true,
        },
      },
    },
  });
}

async function getSourcesByProvider(kc_id) {
  if (!kc_id) {
    throw new Error("Missing required parameter: kc_id");
  }

  return await prisma.source.findMany({
    where: {
      providers: {
        some: {
          user: {
            kc_id,
          },
        },
      },
    },
    include: {
      source_indices: true,
      providers: {
        include: {
          user: true,
        },
      },
    },
  });
}

async function getIndexedSourcesByProvider(kc_id) {
  if (!kc_id) {
    throw new Error("Missing required parameter: kc_id");
  }

  return await prisma.source.findMany({
    where: {
      providers: {
        some: {
          user: {
            kc_id,
          },
        },
      },
      source_indices: {
        some: {},
      },
    },
    include: {
      source_indices: true,
      providers: {
        include: {
          user: true,
        },
      },
    },
  });
}

async function getIndexedSources() {
  return await prisma.source.findMany({
    where: {
      source_indices: {
        some: {},
      },
    },
    include: {
      source_indices: true,
      providers: {
        include: {
          user: true,
        },
      },
    },
  });
}

async function createSource(name, description, metaUrfms, kc_id) {
  if (!name) {
    throw new Error("Missing required parameter: name");
  }

  if (!description) {
    throw new Error("Missing required parameter: description");
  }

  if (!kc_id) {
    throw new Error("Missing required parameter: kc_id");
  }

  const user = await prisma.user.findUnique({
    where: {
      kc_id,
    },
  });

  if (!user) {
    throw new Error("User not found");
  }

  const existingSource = await prisma.source.findFirst({
    where: {
      name,
    },
  });

  if (existingSource) {
    throw new Error("Source already exists");
  }

  if (!metaUrfms) {
    throw new Error("Missing required parameter: metaUrfms");
  }

  const metadata = {
    name,
    description,
    metaUrfms,
  };

  const result = await addSourceToMongoDB(metadata);

  if (!result) {
    throw new Error("Failed to create source");
  }
  const { insertedId } = result;

  logger.debug(`Inserted source with id: ${insertedId}`);
  const elkIndex = `${name.split(/\s/).join("").toLowerCase()}-${insertedId}`;
  logger.debug(`Indexing documents with index: ${elkIndex}`);

  const body = await prepareForBulk(metaUrfms, elkIndex);
  try {
    const success = await bulk(body);
    if (!success) {
      throw new Error("Failed to index documents");
    }

    const newSource = await prisma.source.create({
      data: {
        name,
        description,
        source_indices: {
          create: {
            mng_id: insertedId,
            index_id: elkIndex,
            is_send: true,
          },
        },
      },
      include: {
        source_indices: true,
        providers: {
          include: {
            user: true,
          },
        },
      },
    });

    logger.debug(`Created source with id: ${newSource.id}`);

    await prisma.sourceUser.create({
      data: {
        source_id: newSource.id,
        user_id: user.id,
      },
    });

    logger.debug(
      `Assigned source with id: ${newSource.id} to provider with id: ${user.id}`
    );

    return newSource;
  } catch (error) {
    logger.error("Error while inserting in elastic", error);
    await removeSourceFromMongoDB(insertedId);
    await deleteIndex(elkIndex);
    const sourceToDelete = await prisma.source.findUnique({
      where: {
        source_indices: {
          some: {
            mng_id: insertedId,
          },
        },
      },
    });
    await prisma.source.deleteMany({
      where: {
        id: sourceToDelete.id,
      },
    });
    await prisma.sourceUser.deleteMany({
      where: {
        source_id: sourceToDelete.id,
      },
    });
    throw new Error("Failed to create source");
  }
}

async function deleteSource(id) {
  if (!id) {
    throw new Error("Missing required parameter: id");
  }
  id = +id;
  logger.debug(`Deleting source with id: ${id}`);
  const source = await prisma.source.findUnique({
    where: {
      id,
    },
    include: {
      source_indices: true,
    },
  });

  if (!source) {
    logger.error(`Source with id: ${id} not found`);
    throw new Error("Source not found");
  }
  logger.debug(`Found source with id: ${id}`);
  const source_indices = source.source_indices.length;
  if (source_indices > 0) {
    const { mng_id, index_id } = source.source_indices[0];

    if (mng_id) {
      await removeSourceFromMongoDB(mng_id);
    }
    if (index_id) {
      await deleteIndex(index_id);
    }
  }

  await prisma.source.delete({
    where: {
      id,
    },
  });

  logger.debug(`Deleted source with id: ${id}`);
  return true;
}

module.exports = {
  getSources,
  getSource,
  getSourcesByProvider,
  getIndexedSourcesByProvider,
  createSource,
  deleteSource,
  getIndexedSources,
};
