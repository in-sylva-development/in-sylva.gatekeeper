"use strict";

const prisma = require("@/prisma/client");

async function createRole(name, description) {
  if (!name || !description) {
    throw Error("The request body is empty!");
  }

  const role = await prisma.role.findFirst({
    where: {
      name: name,
    },
  });

  if (role) {
    throw Error(`This Role name:${name} already inserted to database!`);
  } else {
    const newRole = await prisma.role.create({
      data: {
        name: name,
        description: description,
      },
    });

    return newRole;
  }
}

async function getRoles() {
  return await prisma.role.findMany({
    include: {
      users: {
        include: {
          user: true,
        },
      },
    },
  });
}

async function getRole(id) {
  if (!id) {
    throw Error("The request body is empty!");
  }
  const role = await prisma.role.findFirst({
    where: {
      id: +id,
    },
    include: {
      users: {
        include: {
          user: true,
        },
      },
    },
  });
  return role;
}

async function getUserRoles(kc_id) {
  if (!kc_id) {
    throw Error("The request body is empty!");
  }

  const user = await prisma.role.findMany({
    where: {
      users: {
        some: {
          kc_id: kc_id,
        },
      },
    },
  });

  return user.roles;
}

async function deleteRole(id) {
  if (!id) {
    throw Error("The request body is empty!");
  }

  const role = await prisma.role.findFirst({
    where: {
      id: +id,
    },
  });

  if (!role) {
    throw Error("The role could not be found.");
  }

  const deletedRole = await prisma.role.delete({
    where: {
      id: +id,
    },
  });

  return deletedRole;
}

async function updateRole(id, name, description) {
  if (!id || !name || !description) {
    throw Error("The request body is empty!");
  }

  const role = await prisma.role.findFirst({
    where: {
      id: +id,
    },
  });

  if (!role) {
    throw Error("The role could not be found.");
  }

  const updatedRole = await prisma.role.update({
    where: {
      id: +id,
    },
    data: {
      name: name,
      description: description,
    },
  });

  return updatedRole;
}

async function addUserToRole(kc_id, role_id) {
  if (!kc_id || !role_id) {
    throw Error("The request body is empty!");
  }

  const user = await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
  });

  if (!user) {
    throw Error("The user account could not be found.");
  }

  const role = await prisma.role.findFirst({
    where: {
      id: +role_id,
    },
  });

  if (!role) {
    throw Error("The role could not be found.");
  }

  const userRole = await prisma.roleUser.findFirst({
    where: {
      user_id: user.id,
      role_id: role.id,
    },
  });

  if (userRole) {
    throw Error("The user is already assigned to this role.");
  }

  const updatedRole = await prisma.roleUser.create({
    data: {
      role_id: role.id,
      user_id: user.id,
    },
  });

  return updatedRole;
}

async function removeUserFromRole(kc_id, role_id) {
  if (!kc_id || !role_id) {
    throw Error("The request body is empty!");
  }

  const user = await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
  });

  if (!user) {
    throw Error("The user account could not be found.");
  }

  const role = await prisma.role.findFirst({
    where: {
      id: +role_id,
    },
  });

  if (!role) {
    throw Error("The role could not be found.");
  }

  const updatedRole = await prisma.roleUser.delete({
    where: {
      role_id_user_id: {
        role_id: role.id,
        user_id: user.id,
      },
    },
  });

  return updatedRole;
}

module.exports = {
  createRole,
  getRoles,
  getRole,
  getUserRoles,
  deleteRole,
  updateRole,
  addUserToRole,
  removeUserFromRole,
};
