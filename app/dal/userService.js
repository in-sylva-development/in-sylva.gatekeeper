"use strict";

const prisma = require("../../prisma/client");
const { deleteKeycloakUser } = require("../dal/keycloakService");

async function getUsers() {
  return await prisma.user.findMany({
    include: {
      roles: {
        include: {
          role: true,
        },
      },
      groups: {
        include: {
          group: true,
        },
      },
      group_admin: true,
    },
  });
}

async function getUser(kc_id) {
  if (!kc_id) {
    return null;
  }

  return await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
    include: {
      roles: {
        include: {
          role: true,
        },
      },
      groups: {
        include: {
          group: {
            include: {
              policies: true,
            },
          },
        },
      },
      group_admin: true,
      requests: true,
      display_fields: {
        include: {
          std_field: true,
        },
      },
    },
  });
}

async function createUser(kc_id, email) {
  if (!kc_id || !email) {
    throw Error("The request body is empty!");
  }

  const user = await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
  });

  if (user) {
    throw Error("User already exists!");
  }

  const newUser = await prisma.user.create({
    data: {
      kc_id: kc_id,
      email,
    },
  });

  await prisma.roleUser.create({
    data: {
      role_id: 3,
      user_id: newUser.id,
    },
  });

  return newUser;
}

async function deleteUser(kc_id) {
  if (!kc_id) {
    throw Error("The request body is empty!");
  }

  const user = await prisma.user.findFirst({
    where: {
      kc_id: kc_id,
    },
  });

  if (!user) {
    throw Error("User not found");
  }

  const deleted = await prisma.user.delete({
    where: {
      kc_id: kc_id,
    },
  });
  if (!deleted) {
    throw Error("User not found!");
  }
  const kcDeleted = await deleteKeycloakUser({ kc_id: kc_id });
  if (!kcDeleted) {
    throw Error("Keycloak user not found!");
  }
  return deleted;
}

module.exports = {
  getUsers,
  getUser,
  createUser,
  deleteUser,
};
