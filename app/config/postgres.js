"use strict";

const joi = require("@hapi/joi");

const envVarsSchema = joi
  .object({
    DATABASE_URL: joi.string().required(),
  })
  .unknown()
  .required();

joi.attempt(process.env, envVarsSchema);
