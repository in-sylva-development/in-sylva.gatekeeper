"use strict";
const joi = require("@hapi/joi");

const envVarsSchema = joi
  .object({
    MONGO_PORT: joi.number().required(),
    MONGO_HOST: joi.string().required(),
    MONGO_DB_NAME: joi.string().required(),
    MONGO_USERNAME: joi.string().required(),
    MONGO_PASSWORD: joi.string().required(),
  })
  .unknown()
  .required();

const env = joi.attempt(process.env, envVarsSchema);

const MongoClient = require("mongodb").MongoClient;
const authMechanism = "DEFAULT";
const url = `mongodb://${env.MONGO_USERNAME}:${env.MONGO_PASSWORD}@${env.MONGO_HOST}:${env.MONGO_PORT}/?authMechanism=${authMechanism}`;
const mongoClient = new MongoClient(url);

module.exports = {
  mongoClient,
  dbName: env.MONGO_DB_NAME,
};
