"use strict";
const joi = require("@hapi/joi");

const envVarsSchema = joi
  .object({
    ELK_URL: joi.string().required(),
    ELK_USERNAME: joi.string().required(),
    ELK_PASSWORD: joi.string().required(),
  })
  .unknown()
  .required();

const configElk = joi.attempt(process.env, envVarsSchema);

module.exports = {
  configElk,
};
