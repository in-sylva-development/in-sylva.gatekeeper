"use strict";

const {
  deleteHistory,
  deleteAllHistoryByUser,
} = require("@/app/dal/searchHistoryService");

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

async function deleteHistoryHandler(ctx) {
  const { id } = ctx.params;
  if (!id) {
    handleMissingParameters(["id"], ctx);
  } else {
    try {
      await deleteHistory(id);
      ctx.status = 204;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

async function deleteAllHistoryByUserHandler(ctx) {
  const { kc_id } = ctx.params;
  if (!kc_id) {
    handleMissingParameters(["kc_id"], ctx);
  } else {
    try {
      await deleteAllHistoryByUser(kc_id);
      ctx.status = 204;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

module.exports = {
  deleteHistoryHandler,
  deleteAllHistoryByUserHandler,
};
