"use strict";

const { getSearchHistoryByUser } = require("@/app/dal/searchHistoryService");
const {
  handleMissingParameters,
  handleInternalError,
} = require("@/app/utils/errorHandler");

async function getHistoryHandler(ctx) {
  const { kc_id } = ctx.params;
  if (!kc_id) {
    handleMissingParameters(["kc_id"], ctx);
  } else {
    try {
      ctx.body = await getSearchHistoryByUser(kc_id);
      ctx.status = 200;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

module.exports = {
  getHistoryHandler,
};
