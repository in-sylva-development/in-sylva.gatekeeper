"use strict";

const { addSearchHistoryToUser } = require("@/app/dal/searchHistoryService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

async function addSearchHistoryToUserHandler(ctx) {
  const { kc_id } = ctx.params;
  const { query, name, ui_structure, description } = ctx.request.body;
  if (!kc_id || !query || !name || !ui_structure || !description) {
    const missingParameters = [];
    if (!kc_id) {
      missingParameters.push("kc_id");
    }
    if (!query) {
      missingParameters.push("query");
    }
    if (!name) {
      missingParameters.push("name");
    }
    if (!ui_structure) {
      missingParameters.push("ui_structure");
    }
    if (!description) {
      missingParameters.push("description");
    }
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await addSearchHistoryToUser(
        kc_id,
        query,
        name,
        ui_structure,
        description
      );
      ctx.status = 201;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

module.exports = {
  addSearchHistoryToUserHandler,
};
