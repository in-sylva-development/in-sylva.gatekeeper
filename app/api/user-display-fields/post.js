const {
  handleInternalError,
  handleMissingParameters,
} = require("../../utils/errorHandler");
const { setUserDisplayFields } = require("../../dal/userDisplayFieldsService");

async function setUserDisplayFieldsHandler(ctx) {
  const { kc_id } = ctx.params;
  const { fields_id } = ctx.request.body;
  if (!kc_id || !fields_id) {
    const missingParams = [];
    if (!kc_id) missingParams.push("kc_id");
    if (!fields_id) missingParams.push("fields_id");
    handleMissingParameters(missingParams, ctx);
  } else {
    try {
      ctx.body = await setUserDisplayFields(kc_id, fields_id);
      ctx.status = 201;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

module.exports = {
  setUserDisplayFieldsHandler,
};
