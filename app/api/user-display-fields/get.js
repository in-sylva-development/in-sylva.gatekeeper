const { getUserDisplayFields } = require("../../dal/userDisplayFieldsService");
const {
  handleInternalError,
  handleMissingParameters,
} = require("../../utils/errorHandler");

async function getUserDisplayFieldsHandler(ctx) {
  const { kc_id } = ctx.params;
  if (!kc_id) {
    const missingParams = [];
    if (!kc_id) missingParams.push("kc_id");
    handleMissingParameters(missingParams, ctx);
  } else {
    try {
      ctx.body = await getUserDisplayFields(kc_id);
      ctx.status = 200;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

module.exports = {
  getUserDisplayFieldsHandler,
};
