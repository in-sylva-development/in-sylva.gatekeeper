"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const { deleteUser } = require("@/app/dal/userService");

async function deleteUserHandler(ctx) {
  const { kc_id } = ctx.params;
  if (!kc_id) {
    handleMissingParameters(["kc_id"], ctx);
  } else {
    try {
      ctx.body = await deleteUser(kc_id);
      ctx.status = 204;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

module.exports = {
  deleteUserHandler,
};
