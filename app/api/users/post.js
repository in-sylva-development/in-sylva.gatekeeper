"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");

const { createUser, getUser } = require("@/app/dal/userService");

async function getOrCreateUserHandler(ctx) {
  const { kc_id, email } = ctx.request.body;
  if (!kc_id || !email) {
    const missingParameters = [];
    if (!kc_id) {
      missingParameters.push("kc_id");
    }
    if (!email) {
      missingParameters.push("email");
    }
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      const user = await getUser(kc_id);
      if (user) {
        ctx.body = user;
        ctx.status = 200;
      } else {
        ctx.body = await createUser(kc_id, email);
        ctx.status = 201;
      }
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

module.exports = {
  getOrCreateUserHandler,
};
