"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const { getUsers, getUser } = require("@/app/dal/userService");

async function getUsersHandler(ctx) {
  try {
    ctx.body = await getUsers();
    ctx.status = 200;
  } catch (err) {
    handleInternalError(err, ctx);
  }
}

async function getUserHandler(ctx) {
  const { params } = ctx;
  const { kc_id } = params;
  if (!kc_id) {
    handleMissingParameters(["kc_id"], ctx);
  } else {
    try {
      const user = await getUser(kc_id);
      if (!user) {
        ctx.status = 404;
        ctx.body = { message: "User not found" };
      } else {
        ctx.body = user;
        ctx.status = 200;
      }
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

module.exports = {
  getUsersHandler,
  getUserHandler,
};
