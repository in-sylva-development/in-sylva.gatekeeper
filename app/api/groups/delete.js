"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const { deleteGroup } = require("@/app/dal/groupService");

async function deleteGroupHandler(ctx) {
  const { id } = ctx.params;
  if (!id) {
    handleMissingParameters(["id"], ctx);
  } else {
    try {
      await deleteGroup(id);
      ctx.status = 204;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

module.exports = {
  deleteGroupHandler,
};
