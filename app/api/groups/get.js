"use strict";

const {
  handleMissingParameters,
  handleInternalError,
} = require("@/app/utils/errorHandler");
const { getAllGroups, getGroup } = require("@/app/dal/groupService");

async function getGroupHandler(ctx) {
  const { id } = ctx.params;
  if (!id) {
    handleMissingParameters(["id"], ctx);
  }
  try {
    ctx.body = await getGroup(id);
    ctx.status = 200;
  } catch (error) {
    handleInternalError(error, ctx);
  }
}

async function getAllGroupsHandler(ctx) {
  try {
    ctx.body = await getAllGroups();
    ctx.status = 200;
  } catch (error) {
    handleInternalError(error, ctx);
  }
}

module.exports = {
  getAllGroupsHandler,
  getGroupHandler,
};
