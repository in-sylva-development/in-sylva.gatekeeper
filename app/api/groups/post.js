"use strict";

const {
  handleMissingParameters,
  handleInternalError,
} = require("@/app/utils/errorHandler");
const { createGroup } = require("@/app/dal/groupService");

async function createGroupHandler(ctx) {
  const { name, description, kc_id } = ctx.request.body;
  if (!name || !description || !kc_id) {
    const missingParameters = [];
    if (!name) missingParameters.push("name");
    if (!description) missingParameters.push("description");
    if (!kc_id) missingParameters.push("kc_id");
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await createGroup(name, description, kc_id);
      ctx.status = 201;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

module.exports = {
  createGroupHandler,
};
