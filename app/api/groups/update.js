"use strict";

const {
  handleMissingParameters,
  handleInternalError,
} = require("@/app/utils/errorHandler");
const {
  addUserToGroup,
  addPolicyToGroup,
  removePolicyFromGroup,
  removeUserFromGroup,
  updateGroup,
} = require("@/app/dal/groupService");

async function updateGroupHandler(ctx) {
  const { id } = ctx.params;
  const { name, description } = ctx.request.body;
  if (!name || !description || !id) {
    const missingParameters = [];
    if (!name) missingParameters.push("name");
    if (!description) missingParameters.push("description");
    if (!id) missingParameters.push("id");
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await updateGroup(id, name, description);
      ctx.status = 200;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

async function addUserToGroupHandler(ctx) {
  const { id } = ctx.params;
  const { kc_id } = ctx.request.body;
  if (!kc_id || !id) {
    const missingParameters = [];
    if (!kc_id) missingParameters.push("kc_id");
    if (!id) missingParameters.push("id");
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await addUserToGroup(kc_id, id);
      ctx.status = 201;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

async function removeUserFromGroupHandler(ctx) {
  const { id, kc_id } = ctx.params;
  if (!kc_id || !id) {
    const missingParameters = [];
    if (!kc_id) missingParameters.push("kc_id");
    if (!id) missingParameters.push("id");
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      await removeUserFromGroup(kc_id, id);
      ctx.status = 204;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

async function addPolicyToGroupHandler(ctx) {
  const { id } = ctx.params;
  const { policy_id } = ctx.request.body;
  if (!policy_id || !id) {
    const missingParameters = [];
    if (!policy_id) missingParameters.push("policy_id");
    if (!id) missingParameters.push("id");
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await addPolicyToGroup(policy_id, id);
      ctx.status = 201;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

async function removePolicyFromGroupHandler(ctx) {
  const { policy_id, id } = ctx.params;
  if (!policy_id || !id) {
    const missingParameters = [];
    if (!policy_id) missingParameters.push("policy_id");
    if (!id) missingParameters.push("id");
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await removePolicyFromGroup(policy_id, id);
      ctx.status = 204;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

module.exports = {
  updateGroupHandler,
  addUserToGroupHandler,
  addPolicyToGroupHandler,
  removeUserFromGroupHandler,
  removePolicyFromGroupHandler,
};
