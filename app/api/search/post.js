const {
    handleInternalError,
    handleMissingParameters,
} = require("@/app/utils/errorHandler");
const {search} = require("@/app/dal/elasticService");
const logger = require("@/app/utils/format");

async function getQueryResultsHandler(ctx) {
    logger.debug("getQueryResultsHandler");
    const {
        query,
        sourcesId,
        fieldsId,
        scroll_id,
        advancedQuery = false,
    } = ctx.request.body;
    if (!query || !sourcesId || !fieldsId) {
        const missingParams = [];
        if (!query) missingParams.push("query");
        if (!sourcesId) missingParams.push("sourcesId");
        if (!fieldsId) missingParams.push("fieldsId");
        handleMissingParameters(missingParams, ctx);
    } else {
        try {
            logger.debug(`query: ${JSON.stringify(query)}`);
            logger.debug(`sourcesId: ${sourcesId}`);
            if (!Array.isArray(sourcesId)) {
                handleInternalError(new Error("Invalid sourcesId"), ctx);
            }
            const allNumbers = sourcesId.every((id) => typeof id === "number");
            if (!allNumbers) {
                handleInternalError(new Error("Invalid sourcesId"), ctx);
            } else {
                const result = await search({
                    query,
                    sourcesId,
                    scroll_id,
                    advancedQuery,
                });
                logger.debug(`result: ${JSON.stringify(result)}`);
                let hits = [];
                for (let record of result?.hits?.hits) {
                    const {_source, _index, _id} = record;
                    logger.debug(`_source: ${JSON.stringify(_source)}`);
                    logger.debug(`_index: ${_index}`);
                    logger.debug(`_id: ${_id}`);
                    hits.push({
                        id: `${_index}_${_id}`,
                        ..._source,
                    });
                }
                ctx.body = hits;
                ctx.status = 200;
            }
        } catch (err) {
            handleInternalError(err, ctx);
        }
    }
}

module.exports = {
    getQueryResultsHandler,
};
