"use strict";

const Router = require("koa-router");
const bodyParser = require("koa-bodyparser");
const routers = new Router();

const { getUsersHandler, getUserHandler } = require("@/app/api/users/get");
const { getOrCreateUserHandler } = require("@/app/api/users/post");
const { deleteUserHandler } = require("@/app/api/users/delete");

const { getHistoryHandler } = require("@/app/api/user-search-history/get");
const {
  addSearchHistoryToUserHandler,
} = require("@/app/api/user-search-history/post");
const {
  deleteHistoryHandler,
  deleteAllHistoryByUserHandler,
} = require("@/app/api/user-search-history/delete");

const {
  getAllRequestsHandler,
  getAllPendingRequestsHandler,
  getAllUserRequestsHandler,
} = require("@/app/api/user-requests/get");
const {
  createRequestHandler,
  updateRequestHandler,
} = require("@/app/api/user-requests/post");
const { deleteRequestHandler } = require("@/app/api/user-requests/delete");

const {
  getAllGroupsHandler,
  getGroupHandler,
} = require("@/app/api/groups/get");
const {
  addUserToGroupHandler,
  removeUserFromGroupHandler,
  addPolicyToGroupHandler,
  updateGroupHandler,
  removePolicyFromGroupHandler,
} = require("@/app/api/groups/update");
const { createGroupHandler } = require("@/app/api/groups/post");
const { deleteGroupHandler } = require("@/app/api/groups/delete");

const {
  getAllPoliciesHandler,
  getPolicyHandler,
} = require("@/app/api/policies/get");
const { createPolicyHandler } = require("@/app/api/policies/post");
const {
  updatePolicyHandler,
  addFieldToPolicyHandler,
  removeFieldFromPolicyHandler,
  addSourceToPolicyHandler,
  removeSourceFromPolicyHandler,
} = require("@/app/api/policies/update");
const { deletePolicyHandler } = require("@/app/api/policies/delete");

const {
  getStdFieldsHandler,
  getPublicStdFieldsHandler,
  getStdFieldHandler,
} = require("@/app/api/std_fields/get");

const {
  getUserDisplayFieldsHandler,
} = require("@/app/api/user-display-fields/get");

const { createOrUpdateStdFieldHandler } = require("@/app/api/std_fields/post");

const { deleteAllStdFieldsHandler } = require("@/app/api/std_fields/delete");

const {
  getSourcesHandler,
  getSourcesByProviderHandler,
  getIndexedSourcesByProviderHandler,
  getIndexedSourcesHandler,
} = require("@/app/api/sources/get");
const { createSourceHandler } = require("@/app/api/sources/post");
const { deleteSourceHandler } = require("@/app/api/sources/delete");

const {
  createRoleHandler,
  addUserToRoleHandler,
  removeUserFromRoleHandler,
} = require("@/app/api/roles/post");
const { getRolesHandler, getRoleHandler } = require("@/app/api/roles/get");
const { updateRoleHandler } = require("@/app/api/roles/put");
const { deleteRoleHandler } = require("@/app/api/roles/delete");

const { getQueryResultsHandler } = require("@/app/api/search/post");
const { setUserDisplayFieldsHandler } = require("./user-display-fields/post");

routers.use(
  bodyParser({ formLimit: "700mb", jsonLimit: "700mb", textLimit: "700mb" })
);

// search
routers.post("/search", getQueryResultsHandler);

// users
routers.get("/users", getUsersHandler);
routers.get("/users/:kc_id", getUserHandler);
routers.post("/users", getOrCreateUserHandler);
routers.delete("/users/:kc_id", deleteUserHandler);

// search-history
routers.get("/users/:kc_id/history", getHistoryHandler);
routers.post("/users/:kc_id/history", addSearchHistoryToUserHandler);
routers.delete("/users/:kc_id/history", deleteAllHistoryByUserHandler);
routers.delete("/users/:kc_id/history/:id", deleteHistoryHandler);

// user requests
routers.get("/user-requests", getAllRequestsHandler);
routers.get("/user-requests/pending", getAllPendingRequestsHandler);
routers.get("/users/:kc_id/requests", getAllRequestsHandler);
routers.get("/users/:kc_id/requests/pending", getAllUserRequestsHandler);
routers.post("/users/:kc_id/requests", createRequestHandler);
routers.post("/user-requests/:id", updateRequestHandler);
routers.delete("/user-requests/:id", deleteRequestHandler);

// user display fields
routers.get("/users/:kc_id/fields", getUserDisplayFieldsHandler);
routers.post("/users/:kc_id/fields", setUserDisplayFieldsHandler);

// groups
routers.get("/groups", getAllGroupsHandler);
routers.get("/groups/:id", getGroupHandler);
routers.post("/groups", createGroupHandler);
routers.put("/groups/:id", updateGroupHandler);
routers.delete("/groups/:id", deleteGroupHandler);

// groups users
routers.post("/groups/:id/users", addUserToGroupHandler);
routers.delete("/groups/:id/users/:kc_id", removeUserFromGroupHandler);

// groups policies
routers.post("/groups/:id/policies", addPolicyToGroupHandler);
routers.delete("/groups/:id/policies/:policy_id", removePolicyFromGroupHandler);

// policy
routers.get("/policies", getAllPoliciesHandler);
routers.get("/policies/:id", getPolicyHandler);
routers.post("/policies", createPolicyHandler);
routers.put("/policy/update", updatePolicyHandler);
routers.delete("/policies/:id", deletePolicyHandler);

// policy std_fields
routers.post("/policies/:id/std_fields", addFieldToPolicyHandler);
routers.delete(
  "/policies/:id/std_fields/:field_id",
  removeFieldFromPolicyHandler
);

// policy sources
routers.post("/policies/:id/sources", addSourceToPolicyHandler);
routers.delete(
  "/policies/:id/sources/:source_id",
  removeSourceFromPolicyHandler
);

// std_fields
routers.get("/std_fields", getStdFieldsHandler);
routers.get("/public_std_fields", getPublicStdFieldsHandler);
routers.get("/std_fields/:id", getStdFieldHandler);
routers.post("/std_fields", createOrUpdateStdFieldHandler);
routers.put("/std_fields/:id", createOrUpdateStdFieldHandler);
routers.delete("/std_fields", deleteAllStdFieldsHandler);

// sources
routers.get("/sources", getSourcesHandler);
routers.get("/sources/:kc_id", getSourcesByProviderHandler);
routers.post("/sources", createSourceHandler);
routers.get("/indexed-sources", getIndexedSourcesHandler);
routers.get("/indexed-sources/:kc_id", getIndexedSourcesByProviderHandler);
routers.delete("/sources/:id", deleteSourceHandler);

// roles
routers.get("/roles", getRolesHandler);
routers.get("/roles/:id", getRoleHandler);
routers.post("/roles", createRoleHandler);
routers.put("/roles/:id", updateRoleHandler);
routers.delete("/roles/:id", deleteRoleHandler);

// role users
routers.post("/roles/:id/users", addUserToRoleHandler);
routers.delete("/roles/:id/users/:kc_id", removeUserFromRoleHandler);

module.exports = routers;
