"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const {
  updatePolicy,
  addFieldToPolicy,
  removeFieldFromPolicy,
  addSourceToPolicy,
  removeSourceFromPolicy,
} = require("@/app/dal/policyService");

async function updatePolicyHandler(ctx) {
  const { id } = ctx.params;
  const { name, isDefault } = ctx.request.body;
  if (!id || !name || !isDefault) {
    const missingParameters = [];
    if (!id) missingParameters.push("id");
    if (!name) missingParameters.push("name");
    if (!isDefault) missingParameters.push("isDefault");
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await updatePolicy(id, name, isDefault);
      ctx.status = 204;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

async function addFieldToPolicyHandler(ctx) {
  const { id } = ctx.params;
  const { field_id } = ctx.request.body;
  if (!id || !field_id) {
    const missingParameters = [];
    if (!id) missingParameters.push("id");
    if (!field_id) missingParameters.push("field_id");
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await addFieldToPolicy(field_id, id);
      ctx.status = 201;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

async function removeFieldFromPolicyHandler(ctx) {
  const { id, field_id } = ctx.params;
  if (!id || !field_id) {
    const missingParameters = [];
    if (!id) missingParameters.push("id");
    if (!field_id) missingParameters.push("field_id");
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await removeFieldFromPolicy(field_id, id);
      ctx.status = 204;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

async function addSourceToPolicyHandler(ctx) {
  const { id } = ctx.params;
  const { source_id } = ctx.request.body;
  if (!id || !source_id) {
    const missingParameters = [];
    if (!id) missingParameters.push("id");
    if (!source_id) missingParameters.push("source_id");
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await addSourceToPolicy(source_id, id);
      ctx.status = 201;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

async function removeSourceFromPolicyHandler(ctx) {
  const { id, source_id } = ctx.params;
  if (!id || !source_id) {
    const missingParameters = [];
    if (!id) missingParameters.push("id");
    if (!source_id) missingParameters.push("source_id");
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      await removeSourceFromPolicy(source_id, id);
      ctx.status = 204;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

module.exports = {
  updatePolicyHandler,
  addFieldToPolicyHandler,
  removeFieldFromPolicyHandler,
  addSourceToPolicyHandler,
  removeSourceFromPolicyHandler,
};
