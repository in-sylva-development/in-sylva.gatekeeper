"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const { createPolicy } = require("@/app/dal/policyService");

async function createPolicyHandler(ctx) {
  const { name, kc_id } = ctx.request.body;
  if (!name || !kc_id) {
    const missingParameters = [];
    if (!name) missingParameters.push("name");
    if (!kc_id) missingParameters.push("kc_id");
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await createPolicy(name, kc_id);
      ctx.status = 201;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

module.exports = {
  createPolicyHandler,
};
