"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const { getAllPolicies, getPolicy } = require("@/app/dal/policyService");

async function getAllPoliciesHandler(ctx) {
  try {
    ctx.body = await getAllPolicies();
    ctx.status = 200;
  } catch (error) {
    handleInternalError(error, ctx);
  }
}

async function getPolicyHandler(ctx) {
  const { id } = ctx.params;
  if (!id) {
    handleMissingParameters(["id"], ctx);
  } else {
    try {
      ctx.body = await getPolicy(id);
      ctx.status = 200;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

module.exports = {
  getAllPoliciesHandler,
  getPolicyHandler,
};
