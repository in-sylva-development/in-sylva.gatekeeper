"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const { deletePolicy } = require("@/app/dal/policyService");

async function deletePolicyHandler(ctx) {
  const { id } = ctx.params;
  if (!id) {
    handleMissingParameters(["id"], ctx);
  } else {
    try {
      ctx.body = await deletePolicy(id);
      ctx.status = 204;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

module.exports = {
  deletePolicyHandler,
};
