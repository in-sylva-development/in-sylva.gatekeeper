"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const { createOrUpdateStdField } = require("@/app/dal/stdFieldService");

async function updateStdFieldHandler(ctx) {
  const { id } = ctx.params;
  if (!id) {
    const missingParameters = [];
    if (!id) missingParameters.push("id");
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await createOrUpdateStdField(ctx.request.body);
      ctx.status = 204;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

module.exports = {
  updateStdFieldHandler,
};
