"use strict";

const {handleInternalError} = require("@/app/utils/errorHandler");
const {createOrUpdateStdField} = require("@/app/dal/stdFieldService");

async function createOrUpdateStdFieldHandler(ctx) {
    try {
        ctx.body = await createOrUpdateStdField(ctx.request.body);
        ctx.status = 201;
    } catch (error) {
        handleInternalError(error, ctx);
    }
}

module.exports = {
    createOrUpdateStdFieldHandler,
};
