"use strict";

const { handleInternalError } = require("@/app/utils/errorHandler");
const { deleteAllStdFields } = require("@/app/dal/stdFieldService");

async function deleteAllStdFieldsHandler(ctx) {
  try {
    ctx.body = await deleteAllStdFields();
    ctx.status = 204;
  } catch (error) {
    handleInternalError(error, ctx);
  }
}

module.exports = {
  deleteAllStdFieldsHandler,
};
