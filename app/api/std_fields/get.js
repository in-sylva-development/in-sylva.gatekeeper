"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const {
  getStdFields,
  getStdField,
  getPublicStdFields,
} = require("@/app/dal/stdFieldService");

async function getStdFieldsHandler(ctx) {
  try {
    ctx.body = await getStdFields();
    ctx.status = 200;
  } catch (err) {
    handleInternalError(err, ctx);
  }
}

async function getPublicStdFieldsHandler(ctx) {
  try {
    ctx.body = await getPublicStdFields();
    ctx.status = 200;
  } catch (err) {
    handleInternalError(err, ctx);
  }
}

async function getStdFieldHandler(ctx) {
  const { id } = ctx.params;
  if (!id) {
    handleMissingParameters(["id"], ctx);
  } else {
    try {
      ctx.body = await getStdField(id);
      ctx.status = 200;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

module.exports = {
  getStdFieldsHandler,
  getPublicStdFieldsHandler,
  getStdFieldHandler,
};
