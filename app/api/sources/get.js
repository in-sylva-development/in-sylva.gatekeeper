"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const {
  getSources,
  getSource,
  getSourcesByProvider,
  getIndexedSourcesByProvider,
  getIndexedSources,
} = require("@/app/dal/sourceService");

async function getSourcesHandler(ctx) {
  try {
    ctx.body = await getSources();
    ctx.status = 200;
  } catch (err) {
    handleInternalError(err, ctx);
  }
}

async function getSourceHandler(ctx) {
  const { id } = ctx.params;
  if (!id) {
    handleMissingParameters(["id"], ctx);
  } else {
    try {
      ctx.body = await getSource(id);
      ctx.status = 200;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

async function getSourcesByProviderHandler(ctx) {
  const { kc_id } = ctx.params;
  if (!kc_id) {
    handleMissingParameters(["kc_id"], ctx);
  } else {
    try {
      ctx.body = await getSourcesByProvider(kc_id);
      ctx.status = 200;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

async function getIndexedSourcesByProviderHandler(ctx) {
  const { kc_id } = ctx.params;
  if (!kc_id) {
    handleMissingParameters(["kc_id"], ctx);
  } else {
    try {
      ctx.body = await getIndexedSourcesByProvider(kc_id);
      ctx.status = 200;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

async function getIndexedSourcesHandler(ctx) {
  try {
    ctx.body = await getIndexedSources();
    ctx.status = 200;
  } catch (err) {
    handleInternalError(err, ctx);
  }
}

module.exports = {
  getSourcesHandler,
  getSourceHandler,
  getSourcesByProviderHandler,
  getIndexedSourcesByProviderHandler,
  getIndexedSourcesHandler,
};
