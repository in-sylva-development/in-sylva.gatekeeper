"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const { createSource } = require("@/app/dal/sourceService");

async function createSourceHandler(ctx) {
  const { name, description, metaUrfms, kc_id } = ctx.request.body;
  if (!name || !description || !kc_id || !metaUrfms) {
    const missingParams = [];
    if (!name) {
      missingParams.push("name");
    }
    if (!description) {
      missingParams.push("description");
    }
    if (!kc_id) {
      missingParams.push("kc_id");
    }
    if (!metaUrfms) {
      missingParams.push("metaUrfms");
    }
    handleMissingParameters(missingParams, ctx);
  } else {
    try {
      ctx.body = await createSource(name, description, metaUrfms, kc_id);
      ctx.status = 201;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

module.exports = {
  createSourceHandler,
};
