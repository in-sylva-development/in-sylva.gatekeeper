"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const {
  createRequest,
  updateRequest,
} = require("@/app/dal/userRequestService");

async function createRequestHandler(ctx) {
  const { kc_id } = ctx.params;
  const { message } = ctx.request.body;
  if (!kc_id || !message) {
    const missingParameters = [];
    if (!kc_id) {
      missingParameters.push("kc_id");
    }
    if (!message) {
      missingParameters.push("message");
    }
    handleMissingParameters(missingParameters, ctx);
    console.log("missingParameters", missingParameters);
  } else {
    try {
      ctx.body = await createRequest(kc_id, message);
      ctx.status = 201;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

async function updateRequestHandler(ctx) {
  const { id } = ctx.params;
  const { is_processed } = ctx.request.body;
  if (!id || is_processed === undefined) {
    const missingParameters = [];
    if (!id) {
      missingParameters.push("id");
    }
    if (is_processed === undefined) {
      missingParameters.push("is_processed");
    }
    await handleMissingParameters(missingParameters, ctx);
  } else {
    if (typeof is_processed !== "boolean") {
      await handleMissingParameters(["is_processed must be a boolean"], ctx);
      console.log("is_processed must be a boolean");
    } else {
      try {
        ctx.body = await updateRequest(id, is_processed);
        ctx.status = 201;
      } catch (err) {
        await handleInternalError(err, ctx);
      }
    }
  }
}

module.exports = {
  createRequestHandler,
  updateRequestHandler,
};
