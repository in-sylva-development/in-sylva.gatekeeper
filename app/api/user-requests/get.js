"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const {
  getAllRequests,
  getAllPendingRequests,
} = require("@/app/dal/userRequestService");

async function getAllRequestsHandler(ctx) {
  try {
    ctx.body = await getAllRequests();
    ctx.status = 200;
  } catch (err) {
    handleInternalError(err, ctx);
  }
}

async function getAllPendingRequestsHandler(ctx) {
  try {
    ctx.body = await getAllPendingRequests();
    ctx.status = 200;
  } catch (err) {
    handleInternalError(err, ctx);
  }
}

async function getAllUserRequestsHandler(ctx) {
  const { kc_id } = ctx.params;
  if (!kc_id) {
    handleMissingParameters(["kc_id"], ctx);
  } else {
    try {
      ctx.body = await getAllRequests(kc_id);
      ctx.status = 200;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

async function getAllPendingUserRequestsHandler(ctx) {
  const { kc_id } = ctx.params;
  if (!kc_id) {
    handleMissingParameters(["kc_id"], ctx);
  } else {
    try {
      const kc_id = ctx.params.kc_id;
      ctx.body = await getAllPendingRequests(kc_id);
      ctx.status = 200;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

module.exports = {
  getAllRequestsHandler,
  getAllPendingRequestsHandler,
  getAllUserRequestsHandler,
  getAllPendingUserRequestsHandler,
};
