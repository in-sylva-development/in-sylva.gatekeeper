"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("../../utils/errorHandler");
const { deleteRequest } = require("../../dal/userRequestService");

async function deleteRequestHandler(ctx) {
  const { id } = ctx.params;
  if (!id) {
    handleMissingParameters(["id"], ctx);
  } else {
    try {
      await deleteRequest(id);
      ctx.status = 204;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}
module.exports = {
  deleteRequestHandler,
};
