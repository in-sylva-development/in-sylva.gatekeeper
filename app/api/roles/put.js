"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("../../utils/errorHandler");
const { updateRole } = require("../../dal/roleService");

async function updateRoleHandler(ctx) {
  const { id } = ctx.params;
  const { name, description } = ctx.request.body;
  if (!id || !name || !description) {
    const missingParameters = [];
    if (!id) {
      missingParameters.push("id");
    }
    if (!name) {
      missingParameters.push("name");
    }
    if (!description) {
      missingParameters.push("description");
    }
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await updateRole(id, name, description);
      ctx.status = 204;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

module.exports = {
  updateRoleHandler,
};
