"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const { deleteRole } = require("@/app/dal/roleService");

async function deleteRoleHandler(ctx) {
  const { id } = ctx.params;
  if (!id) {
    handleMissingParameters(["id"], ctx);
  } else {
    try {
      ctx.body = await deleteRole(id);
      ctx.status = 204;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

module.exports = {
  deleteRoleHandler,
};
