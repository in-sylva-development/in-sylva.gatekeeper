"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("../../utils/errorHandler");
const { getRoles, getRole, getUserRoles } = require("../../dal/roleService");

async function getRolesHandler(ctx) {
  try {
    ctx.body = await getRoles();
    ctx.status = 200;
  } catch (err) {
    handleInternalError(err, ctx);
  }
}

async function getRoleHandler(ctx) {
  const { id } = ctx.params;
  if (!id) {
    handleMissingParameters(["id"], ctx);
  } else {
    try {
      ctx.body = await getRole(id);
      ctx.status = 200;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

async function getUserRolesHandler(ctx) {
  const { kc_id } = ctx.request.body;
  if (!kc_id) {
    handleMissingParameters(["kc_id"], ctx);
  } else {
    try {
      ctx.body = await getUserRoles(kc_id);
      ctx.status = 200;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

module.exports = {
  getRolesHandler,
  getRoleHandler,
  getUserRolesHandler,
};
