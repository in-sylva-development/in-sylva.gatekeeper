"use strict";

const {
  handleInternalError,
  handleMissingParameters,
} = require("@/app/utils/errorHandler");
const {
  createRole,
  addUserToRole,
  removeUserFromRole,
} = require("@/app/dal/roleService");

async function createRoleHandler(ctx) {
  const { name, description } = ctx.request.body;
  if (!name || !description) {
    const missingParameters = [];
    if (!name) {
      missingParameters.push("name");
    }
    if (!description) {
      missingParameters.push("description");
    }
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await createRole(name, description);
      ctx.status = 201;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

async function addUserToRoleHandler(ctx) {
  const { id } = ctx.params;
  const { kc_id } = ctx.request.body;
  if (!id || !kc_id) {
    const missingParameters = [];
    if (!id) {
      missingParameters.push("id");
    }
    if (!kc_id) {
      missingParameters.push("kc_id");
    }
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await addUserToRole(kc_id, id);
      ctx.status = 201;
    } catch (err) {
      handleInternalError(err, ctx);
    }
  }
}

async function removeUserFromRoleHandler(ctx) {
  const { id, kc_id } = ctx.params;
  if (!kc_id || !id) {
    const missingParameters = [];
    if (!kc_id) missingParameters.push("kc_id");
    if (!id) missingParameters.push("id");
    handleMissingParameters(missingParameters, ctx);
  } else {
    try {
      ctx.body = await removeUserFromRole(kc_id, id);
      ctx.status = 204;
    } catch (error) {
      handleInternalError(error, ctx);
    }
  }
}

module.exports = {
  createRoleHandler,
  addUserToRoleHandler,
  removeUserFromRoleHandler,
};
