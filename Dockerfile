FROM node:20.16.0

# Update the package list and install packages
RUN wget -qO- https://www.mongodb.org/static/pgp/server-8.0.asc | tee /etc/apt/trusted.gpg.d/server-8.0.asc
RUN echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/8.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-8.0.list

RUN apt-get update && apt-get install -y git mongodb-mongosh && rm -rf /var/lib/apt/lists/*


# Install PM2
RUN yarn global add pm2

# Set up the app
WORKDIR /app
COPY package.json ./

# Install dependencies
RUN yarn install

# Copy the rest of the application code
COPY . .

ENTRYPOINT [ "/bin/bash", "./entrypoint.sh"]

CMD [ "pm2-runtime","--name","in-sylva.gatekeeper", "yarn start:prod"]
